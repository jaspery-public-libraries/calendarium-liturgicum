# Calendarium Liturgicum
> Providers resources and API for liturgical calendar calculations.

## Modules

* aaa
* bbb

## Installing / Getting started

To use most recent snapshot version of libraries in your project, use gradle
code snapshots provided below.

To use jaspery-kotlin-lang:

```gradle
dependencies {
    implementation 'com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:jaspery-kotlin-lang:-SNAPSHOT'
}
```

To use kotlin-testng-dataprovider:

```gradle
dependencies {
    testImplementation 'com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:kotlin-testng-dataprovider:-SNAPSHOT'
}
```

There are no stable releases of these libraries yet.

## Features

### AAA

TBD

### BBB

See [README](kotlin-testng-dataprovider/README.md)

## Deployment / Publishing

Code is hosted on [GitLab](https://gitlab.com/jaspery-public-libraries/jaspery-kotlin-ext)
and published via [JitPack](https://jitpack.io)

## Links

- Project homepage: https://gitlab.com/jaspery-public-libraries/jaspery-kotlin-ext
- Repository: https://gitlab.com/jaspery-public-libraries/jaspery-kotlin-ext
- Related projects:
  - TBD

## Licensing

Pre-release (snapshot) code in this project is licensed under GPL v3.0 license.
This may change in the future.

## About Author

Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
