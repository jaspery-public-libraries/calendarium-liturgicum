/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus

import io.cucumber.testng.AbstractTestNGCucumberTests
import io.cucumber.testng.CucumberOptions
import org.testng.annotations.Test

@Test
@CucumberOptions(
        features = ["src/acceptanceTest/resources/features"],
        tags = "not @ignored",
        plugin = [
            "pretty",
            "html:build/reports/cucumber-reports/cucumber-pretty",
            "json:build/reports/cucumber-reports/CucumberTestReport.json",
            "rerun:build/reports/cucumber-reports/rerun.txt"
        ])
class EasterComputusCukesTestRunner : AbstractTestNGCucumberTests()
