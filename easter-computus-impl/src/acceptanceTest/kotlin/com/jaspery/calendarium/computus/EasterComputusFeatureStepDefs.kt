/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus

import io.cucumber.java8.En
import org.assertj.core.api.Assertions
import org.threeten.bp.LocalDate
import org.threeten.bp.Year

internal class EasterComputusFeatureStepDefs : En {
    private lateinit var easterComputus: EasterComputus
    private lateinit var year: Year

    init {
        Given("Easter Computus Service is initialized with {easter_computus_impl} implementation") { it: EasterComputus ->
            this.easterComputus = it
        }

        When("year is {year}") { it: Year -> this.year = it }

        Then("Easter falls on {local_date}") { date: LocalDate ->
            val easterDate = easterComputus.compute(year)
            Assertions.assertThat(easterDate).isEqualTo(date)
        }

        Then("Julian Easter falls on {local_date}") { date: LocalDate ->
            val easterDateJulian = easterComputus.computeJulian(year)
            Assertions.assertThat(easterDateJulian).isEqualTo(date)
        }

        Then("Easter computation fails") {
            Assertions.assertThatIllegalArgumentException().isThrownBy {
                easterComputus.compute(year)
            }.withMessageMatching("(Year not supported|Cannot calculate easter date before 1583, but).*")
        }

        Then("Julian Easter computation fails") {
            Assertions.assertThatIllegalArgumentException().isThrownBy {
                easterComputus.computeJulian(year)
            }.withMessageMatching("(Year not supported|Cannot calculate easter date before 1583, but).*")
        }
    }
}
