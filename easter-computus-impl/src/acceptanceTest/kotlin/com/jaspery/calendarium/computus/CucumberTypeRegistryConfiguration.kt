/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus

import com.jaspery.calendarium.computus.impl.gauss.GaussEasterComputus
import com.jaspery.calendarium.computus.impl.tabular.MapBasedEasterComputus
import io.cucumber.core.api.TypeRegistry
import io.cucumber.core.api.TypeRegistryConfigurer
import io.cucumber.cucumberexpressions.ParameterType
import org.threeten.bp.LocalDate
import org.threeten.bp.Year
import java.util.*

class CucumberTypeRegistryConfiguration : TypeRegistryConfigurer {
    override fun configureTypeRegistry(typeRegistry: TypeRegistry?) {
        typeRegistry?.defineParameterType(
                ParameterType("year", ".*?", Year::class.java) { it: String -> Year.parse(it) }
        )
        typeRegistry?.defineParameterType(
                ParameterType("local_date", "\\d{4}-[01]\\d-[0-3]\\d", LocalDate::class.java) { it: String ->
                    LocalDate.parse(it)
                }
        )
        typeRegistry?.defineParameterType(
                ParameterType("easter_computus_impl", ".*?", EasterComputus::class.java) { it: String ->
                    easterComputusImplByName(it)
                }
        )
    }

    private fun easterComputusImplByName(easterComputusImplName: String) = when (easterComputusImplName) {
        "Map-Based" -> MapBasedEasterComputus.instance
        "Gauss" -> GaussEasterComputus()
        else -> throw IllegalArgumentException("Unsupported easter computus implementation $easterComputusImplName")
    }

    override fun locale(): Locale = Locale.ENGLISH
}
