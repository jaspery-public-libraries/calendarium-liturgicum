Feature: Easter Computus
  As a developer
  I want to have service which computes Easter date
  So that I can compute liturgical calendar temporal cycle and sanctoral calendar for different years

  Scenario: Easter in 2018
    Given Easter Computus Service is initialized with Map-Based implementation
    When year is 2018
    Then Easter falls on 2018-04-01

  Scenario Outline: Easter Computus in 21st century
    Given Easter Computus Service is initialized with <easter_computus_impl> implementation
    When year is <year>
    Then Easter falls on <easter_date>
    And Julian Easter falls on <easter_date_julian>

    Examples:
      | easter_computus_impl | year | easter_date | easter_date_julian |

      | Map-Based            | 2017 | 2017-04-16  | 2017-04-16         |
      | Map-Based            | 2018 | 2018-04-01  | 2018-04-08         |
      | Map-Based            | 2019 | 2019-04-21  | 2019-04-28         |

      | Gauss                | 2017 | 2017-04-16  | 2017-04-16         |
      | Gauss                | 2018 | 2018-04-01  | 2018-04-08         |
      | Gauss                | 2019 | 2019-04-21  | 2019-04-28         |

  Scenario Outline: Map-Based implementation limitations
    Given Easter Computus Service is initialized with Map-Based implementation
    When year is <year>
    Then Easter computation fails
    And Julian Easter computation fails

    Examples:
      | year |
      | 1899 |
      | 2101 |

  Scenario: Gauss implementation limitations
    Given Easter Computus Service is initialized with Gauss implementation
    When year is 1582
    Then Easter computation fails
    And Julian Easter computation fails
