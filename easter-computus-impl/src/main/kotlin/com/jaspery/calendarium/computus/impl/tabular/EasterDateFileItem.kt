/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.tabular

import org.threeten.bp.LocalDate
import org.threeten.bp.Year

data class EasterDateFileItem(val year: Year, val easterDate: LocalDate, val easterDateJulian: LocalDate) {
    companion object {
        private const val EASTER_DATE_FILE_ITEM_PROPERTIES_COUNT = 3

        internal fun parseItem(easterDateFileLine: List<String>): EasterDateFileItem {
            check(easterDateFileLine.size == EASTER_DATE_FILE_ITEM_PROPERTIES_COUNT) {
                "Cannot parse ${EasterDateFileItem::class} from list $easterDateFileLine"
            }
            return EasterDateFileItem(
                    Year.parse(easterDateFileLine.component1()),
                    LocalDate.parse(easterDateFileLine.component2()),
                    LocalDate.parse(easterDateFileLine.component3())
            )
        }
    }
}