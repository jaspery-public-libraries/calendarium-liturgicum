/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus

import org.threeten.bp.LocalDate
import org.threeten.bp.Year

interface EasterComputus {
    /**
     * @param year between 1900 and 2100
     * @return
     */
    fun compute(year: Year): LocalDate

    /**
     * @param year between 1900 and 2100
     * @return
     */
    fun computeJulian(year: Year): LocalDate
}