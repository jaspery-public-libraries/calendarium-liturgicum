# Package com.ihalanyuk.computus.impl.gauss

## Gauss Easter Algorithm (Wiki)

### Source

Note, this is link to particular version of wiki page:
https://en.wikipedia.org/w/index.php?title=Computus&oldid=833608869

### Algorithm

#### Brief

In 1800, the mathematician Carl Friedrich Gauss presented this algorithm for calculating the
date of the Julian or Gregorian Easter and made corrections to one of the steps in 1816 In 1800
he incorrectly stated `p = floor (k/3) = ⌊k/3⌋`. In 1807 he replaced the condition
`(11M + 11) mod 30 < 19` with the simpler `a > 10`. In 1811 he limited his algorithm to the 18th
and 19th centuries only, and stated that 26 April is always replaced with 19 April and 25 April
by 18 April. In 1816 he thanked his student Peter Paul Tittel for pointing out that `p` was
wrong in 1800.

| Expression	               | year = 1777 |
|------------------------------|-------------|
| a = year mod 19              | a = 10
| b = year mod 4               | b = 1
| c = year mod 7               | c = 6
| k = ⌊year / 100⌋             | k = 17
| p = ⌊(13 + 8k) / 25⌋         | p = 5
| q = ⌊k / 4 ⌋                 | q = 4
| M = (15 − p + k − q) mod 30  | M = 23
| N = (4 + k − q) mod 7        | N = 3
| d = (19a + M) mod 30         | d = 3
| e = (2b + 4c + 6d + N) mod 7 | e = 5

> Gregorian Easter is 22 + d + e March or d + e − 9 April

> In year 1777 this was 30 March

| Additional rules
|--------------------------------------------|
| if d = 29 and e = 6, replace 26 April with 19 April 
| if d = 28, e = 6, and (11M + 11) mod 30 < 19, replace 25 April with 18 April
| For the Julian Easter in the Julian calendar M = 15 and N = 6 (k, p, and q are unnecessary)

An analysis of the Gauss's Easter algorithm is divided into two parts. The first part is the
approximate tracking of the lunar orbiting and the second one is the exact, deterministic
offsetting to obtain a Sunday following the full moon.

#### The First Part

The first part consists of determining the variable `d`, the number of days (counting from
March 22) for the closest following full moon to occur. The formula for `d` contains the terms
`19a` and the constant `M`. `a` is the year's position in the 19-year lunar phase cycle, in
which by assumption the moon's movement relative to earth repeats every 19 calendar years. In
older times, 19 calendar years were equated to 235 lunar months (the Metonic cycle), which is
remarkably close since 235 lunar months are approximately 6939.6813 days and 19 years are on
average 6939.6075 days. The expression `(19a + M) mod 30` repeats every 19 years within each
century as `M` is determined per century. The 19-year cycle has nothing to do with the '19' in
`19a`, it is just a coincidence that another '19' appears. The '19' in `19a` comes from
correcting the mismatch between a calendar year and an integer number of lunar months.
A calendar year (non-leap year) has 365 days and the closest you can come with an integer number
of lunar months is `12 × 29.5 = 354` days. The difference is 11 days, which must be corrected
for by moving the following year's occurrence of a full moon 11 days back. But in modulo 30
arithmetic, subtracting 11 is the same as adding 19, hence the addition of 19 for each year
added, i.e. `19a`.

The `M` in `19a + M` serves to have a correct starting point at the start of each century. It is
determined by a calculation taking the number of leap years up until that century where `k`
inhibits a leap day every 100 years and `q` re-installs it every 400 years, yielding `(k − q)`
as the total number of inhibitions to the pattern of a leap day every four years. Thus we add
`(k − q)` to correct for leap days that never occurred. `p` corrects for the lunar orbit not
being fully describable in integer terms.

The range of days considered for the full moon to determine Easter are 22 March to 20 April—a
30-day range mirrored in the `mod 30` arithmetic of variable `d` and constant `M`, both of which
can have integer values in the range 0 to 29. Once `d` is determined, this is the number of days
to add to 22 March (the earliest full moon allowed by the date range) to obtain the day of the
full moon.

#### The Second Part

The second part is finding `e`, the additional offset days that must be added to the date offset
`d` to make it arrive at a Sunday. Since the week has 7 days, the offset must be in the range
0 to 6 and determined by modulo 7 arithmetic. `e` is determined by calculating
`2b + 4c + 6d + N mod 7`. These constants may seem strange at first, but are quite easily
explainable if we remember that we operate under mod 7 arithmetic. To begin with, `2b + 4c`
ensures that we take care of the fact that weekdays slide for each year. A normal year has 365
days, but `52 × 7 = 364`, so 52 full weeks make up one day too little. Hence, each consecutive
year, the weekday "slides one day forward", meaning if May 6 was a Wednesday one year, it is a
Thursday the following year (disregarding leap years). Both `b` and `c` increases by one for an
advancement of one year (disregarding modulo effects). The expression `2b + 4c` thus increases
by 6 — but remember that this is the same as subtracting 1 mod 7. And to subtract by 1 is
exactly what is required for a normal year – since the weekday slips one day forward we should
compensate one day less to arrive at the correct weekday (i.e. Sunday). For a leap year, `b`
becomes 0 and `2b` thus is 0 instead of 8—which under mod 7, is another subtraction by 1—i.e.,
a total subtraction by 2, as the weekdays after the leap day that year slides forward by two days.

The expression `6d` works the same way. Increasing `d` by some number `y` indicates that the
full moon occurs `y` days later this year, and hence we should compensate `y` days less. Adding
`6d` is mod 7 the same as subtracting `d`, which is the desired operation. Thus, again, we do
subtraction by adding under modulo arithmetic. In total, the variable `e` contains the step from
the day of the full moon to the nearest following Sunday, between 0 and 6 days ahead. The
constant `N` provides the starting point for the calculations for each century and depends on
where Jan 1, year 1 was implicitly located when the Gregorian calendar was constructed.

The expression `d + e` can yield offsets in the range 0 to 35 pointing to possible Easter
Sundays on March 22 to April 26. For reasons of historical compatibility, all offsets of 35 and
some of 34 are subtracted by 7, jumping one Sunday back to the day before the full moon (in
effect using a negative `e` of −1). This means that 26 April is never Easter Sunday and that 19
April is overrepresented. These latter corrections are for historical reasons only and has
nothing to do with the mathematical algorithm.

Using the Gauss's Easter algorithm for years prior to 1583 is historically pointless since the
Gregorian calendar was not utilised for determining Easter before that year. Using the algorithm
far into the future is questionable, since we know nothing about how different churches will
define Easter that far ahead. Easter calculations are based on agreements and conventions, not
on the actual celestial movements nor on indisputable facts of history.

## Gauss Easter Algorithm Explanation (http://www.henk-reints.nl/easter/index.htm)

### Source

Note, links on that site are not book-markable so please navigate manually:
http://www.henk-reints.nl/easter/index.htm

### Algorithms Breakdown

> Gregorian algorithm by Carl Friedrich Gauss (1777-1855).

> Valid for any year since 1583.

> **NOTE**: Many references suggest this algorithm by Gauss is the official Easter dating method and
that he was the first to find a method for calculating the date of Easter. However, this is
absolutely definitely not true! Lilius & Clavius devised their algorithm in the 1570's, two
centuries before Gauss was born in 1777. Only the Lilius/Clavius method and nothing else has
been used and will be used by the Roman Catholic Church since the Gregorian calendar reform in
1582, as it was announced and prescribed in Pope Gregory XIII's Bull "Inter Gravissimas". 

> **Story**. Gauss knew he was born in 1777, on a Wednesday, a bit more than a week before
Ascension Day. That's what his parents could remember, not the precise date. However, he was
eager to know his exact birth date. Apparently he was unaware of the official Easter dating
method used by the Roman Catholic Church (or maybe he simply didn't want to use it or search for
it), so, being a very intelligent mathematician, he simply made his own Easter dating formula,
and then determined Ascension day was the 8th of May, so he was born on the 30th of April. 

> But please keep in mind that Lilius/Clavius is the **ONLY REAL THING** as far as the official
Easter date of the Roman Catholic Church is concerned!

> For the 20st and 21st century M = 24 and N = 5.

1) Explain the variables that only have to do with the (Gregorian) Calendar:

    * The value of `c = year mod 7` takes care of the fact that a non-leap year is 1 day longer
    that 52 weeks, so for the day of the week every date (including March 21) shifts one day per
    year, as does your own birthday. Only if there is a leap day in between then it shifts an
    extra day, which is handled by `b` and `n`: 
 
    * The value of `b = year mod 4` counts the leap days according to the Julian calendar, i.e.
    one leap day every 4 years. 
 
    * The value of `n = (4 + p - p div 4) mod 7` (where `p = year div 100`) has to do with the
    difference in the number of leap days between the Gregorian and the Julian calendar. The 
    Julian calendar has a leap day every 4 years, whilst the Gregorian calendar excludes the 
    100-fold years from being a leap year unless they can be divided by 400. This has to do with
    the actual length of the year, which on the Gregorian Calendar is assumed to be 365.2425
    days (in reality it currently is 365.24219 days). Together, `b` and `n` handle the Gregorian
    leap days.
     
    * Now we will ignore `d` for a while (it will be explained below). Instead of `e` we will
    first look at `e' = (2 x b + 4 x c + n) mod 7`. The result is the number of days from March
    22 until the next Sunday, i.e. if March 22 is a Sunday then `e' = 0`, if it's a Saturday
    then `e' = 1`, etc. until `e' = 6` if March 22 is a Monday. So March `22 + e'` is the first
    Sunday after March 21.  This calculation is remarkably clever of Mr. Gauss! Both `b` and `c`
    usually increment by 1 every year, so adding `2 x b + 4 x c` means adding 6 days every year.
    But in modulo 7 arithmetic, 6 is the same as -1, so effectively it subtracts 1 from the days
    left until the next Sunday. Since March 22 is a day later the next year, it takes 1 day less
    until the next Sunday. And because `b` never exceeds the value of 3 and `c` never becomes
    larger than 6, the value of `e'` correctly handles the leap days!

2) Taking the Moon into account:

    * It appears to be so that 235 lunations (i.e. moon months) are practically equal to 19
    tropical years (a tropical year is the time between the beginning of spring one year and the
    next year). This means that every 19 years the moon phases will occur on the same dates in
    the year. This regularity was discovered by an ancient Greek called Meton and therefore this
    19-year cycle is called the Metonic cycle (or moon cycle). The value of `a = year mod 19` is
    simply the offset (from 0 to 18) of the given year within the corresponding Metonic cycle. 
 
    * Because this equality of 235 lunations and 19 years is not really exact (the difference is
    approximately 2 hours), there is a small shift of about `1 day per 310 years = approx 8 days
    per 25 centuries`. The value of `M` takes care of that shift (as far as I know Gauss did not
    include a calculation of `M` in his algorithm). For calculating `M`, some intermediate
    results are used: `p = year div 100` is simply the century index, `q = (3 x p + 3) div 4`
    takes care of the leap day difference between the Julian and the Gregorian calendar, and
    `r = (8 x p + 13) div 25` actually handles the shift of the Metonic cycle. 
 
    * Now look at `d = (19 x a + M) mod 30`. The number 19 we see here does not have the same
    meaning as the Metonic cycle used to calculate A, but it comes frome the following: a so
    called Moon year of 12 Moon months is a bit more than 354 days, so it is 11 days shorter
    than the Gregorian Solar year of 365.2425 days. This means that the moon phases occur 11
    days earlier every next year and since a Moon month has got 30 days this implies that the
    Moon phases also occur 19 days later, which is the meaning of the 19 in the calculation of 
    `d`. The value of `d` handles the Metonic cycle (using `a`) and the long term shift thereof
    (using `M`) as well as the length of the Lunar month (the 19 and the mod 30). The result
    actually is the number of days (from 0 to 29) to be added to March 21 in order to get the
    date of the first Full Moon in spring (PFM = Paschal Full Moon).
    
    > PFM - Paschal Full Moon - date of the first Full Moon in spring

3) Finally, the first Sunday after this PFM is calculated:

    * We want to find the Sunday after this PFM, so we have to add up to the first Sunday on or
    after `PFM + 1`. Therefore we start with `March 21 + D + 1 = March 22 + D`. In order to find
    the next Sunday, we do more or less the same as we did above with `e'`. But now we have
    added `d` to March 22. This means that the day of the week has advanced `d mod 7` days, so
    using `e'` to find the next Sunday is no longer appropriate. We will have to compensate for
    this extra advance. This is handled by the term `6 x d` in the calculation of `e`. Note that
    adding `d + (6 x d) mod 7` to any date gets you back on the same day of the week. 
 
    * Together, this means that the value of `e = (2 x b + 4 x c + 6 x d + N) mode 7` is just
    the right number that will bring you to the first Sunday after `March 22 + d`, which is
    Easter Sunday. 
 
    * Altogether the Easter date is calculated as `f = (22 + d + e)` March and if `f` becomes
    larger than 31 it will of course rollover to April. 
 
    * Finally, there is one caveat left: The length of a Moon month is not exactly 30 days,
    but 29.53. This means that for large values of `d` we might find a PFM that is one day late
    and if that happens to be a Sunday, then we will end up with an Easter date that is an
    entire week overdue. Therefore the final correction: 
    `if f = 57 or (f = 56 and e = 6 and a > 10) then f = f - 7` must be applied. 
    This should be read as:
        * if the result is April 26 (`f = 57`) then subtract 1 week;
        * if the result is April 25 (`f = 56`) AND the day after PFM is a Monday (`E = 6`) AND
        the year is in the second half of a Metonic cycle (`a > 10`) then subtract 1 week. 
        (Please note the following: `a` ranges from 0 to 18, so the threshold of `a > 10` means 
        `a/18 > 10/18 = 5/9 = 0.55`, being practically equal to the fractional part of the
        length of the Moon month...)
