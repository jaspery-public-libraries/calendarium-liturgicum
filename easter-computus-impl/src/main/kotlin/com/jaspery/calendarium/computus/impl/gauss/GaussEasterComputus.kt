/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.gauss

import com.jaspery.calendarium.computus.EasterComputus
import com.jaspery.kotlin.lang.require
import org.threeten.bp.LocalDate
import org.threeten.bp.Month
import org.threeten.bp.Year

class GaussEasterComputus : EasterComputus {
    override fun compute(year: Year): LocalDate {
        val y = require(year.value, { it >= YEAR_EARLIEST_VALID }) {
            "Cannot calculate easter date before $YEAR_EARLIEST_VALID, but $year provided"
        }

        val ff = computeGaussShiftFromZeroOfMarch(y, false)

        return when {
            ff <= MAR_LENGTH -> LocalDate.of(y, MAR, ff)
            else -> LocalDate.of(y, APR, ff - MAR_LENGTH)
        }
    }

    override fun computeJulian(year: Year): LocalDate {
        val y = require(year.value, { it >= YEAR_EARLIEST_VALID }) {
            "Cannot calculate easter date before $YEAR_EARLIEST_VALID, but $year provided"
        }

        val ff = computeGaussShiftFromZeroOfMarch(y, true)
        // ff is shift from last day of Feb to Easter according to Julian calendar.
        // Adding few days to convert this date to Gregorian.
        val ffj = ff + julianCalendarShift(y)

        return when {
        // Julian easter actually never (almost?) happens in Mar
            ffj <= MAR_LENGTH -> LocalDate.of(y, MAR, ffj)
            ffj <= MAR_APR_LENGTH -> LocalDate.of(y, APR, ffj - MAR_LENGTH)
            else -> LocalDate.of(y, MAY, ffj - MAR_APR_LENGTH)
        }
    }

    internal fun julianCalendarShift(year: Int) = year / 100 - year / 400 - 2

    /**
     * Compute number of days from beginning of March to Easter on that year [y] using Gauss
     * algorithm.
     *
     * @param y year
     * @param fixedMAndN should be `true` for computing Julian Easter date.
     *
     * @return number of days from beginning of March to Easter on that year [y]
     */
    @Suppress("LocalVariableName")
    private fun computeGaussShiftFromZeroOfMarch(y: Int, fixedMAndN: Boolean): Int {
        val a = y % 19 // the offset (from 0 to 18) of the given year within the corresponding Metonic cycle
        val b = y % 4 // count the leap days according to the Julian calendar, i.e. one leap day every 4 years
        val c = y % 7 // week day shifts by 1 day forward each year. b and N handle extra shift for leap years

        val M: Int // handles shift of Metonic cycle (235 lun is not exactly 19 years)
        val N: Int // handles additional weekday shift in leap years
        if (fixedMAndN) {
            M = M_JULIAN
            N = N_JULIAN
        } else {
            val k = y / 100 // century index (century - 1)
            val p = (13 + 8 * k) / 25
            M = (15 - p + k - k / 4) % 30 // handles shift of Metonic cycle (235 lun is not exactly 19 years)
            N = (4 + k - k / 4) % 7 // handles additional weekday shift in leap years
        }

        val d = (19 * a + M) % 30 // Moon phases occur 19 days later every next year
        val e = (2 * b + 4 * c + 6 * d + N) % 7

        val f = 22 + d + e // 22-Mar - is earliest possible Easter date

        // For large values of `d` we might find a PFM that is one day late and if that happens to
        // be a Sunday, then we will end up with an Easter date that is an entire week overdue.
        // This is a correction formula.
        return if (f == 57 || f == 56 && e == 6 && a > 10) f - 7 else f
    }

    companion object {
        private const val M_JULIAN = 15
        private const val N_JULIAN = 6

        private const val MAR_LENGTH = 31
        private const val APR_LENGTH = 30
        private const val MAR_APR_LENGTH = MAR_LENGTH + APR_LENGTH

        private val MAR = Month.MARCH
        private val APR = Month.APRIL
        private val MAY = Month.MAY

        private const val YEAR_EARLIEST_VALID = 1583
    }
}