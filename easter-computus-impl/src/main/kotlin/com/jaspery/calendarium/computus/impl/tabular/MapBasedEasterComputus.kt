/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.tabular

import com.jaspery.calendarium.computus.EasterComputus
import com.jaspery.kotlin.lang.requireValueIn
import com.jaspery.threetenbp.ext.year
import org.threeten.bp.Year

class MapBasedEasterComputus internal constructor(private val easterDatesList: List<EasterDateFileItem>) : EasterComputus {
    private val easterDatesMap by lazy {
        easterDatesList.associateBy { it.year }.withDefault { error("Year not found $it") }
    }

    override fun compute(year: Year) = easterDatesMap.getValue(requireYearInRange(year)).easterDate

    override fun computeJulian(year: Year) = easterDatesMap.getValue(requireYearInRange(year)).easterDateJulian

    private fun requireYearInRange(year: Year): Year {
        return requireValueIn(year, YEAR_SUPPORTED_RANGE) {
            "Year not supported $year"
        }
    }

    companion object {
        private val YEAR_SUPPORTED_RANGE = 1900.year..2100.year

        val instance by lazy {
            MapBasedEasterComputus(
                    readEasterDateFile()
            )
        }

        fun readEasterDateFile() = parseEasterDatesCsv(EasterDatesResourceBrowser.easterDatesResource)
    }
}
