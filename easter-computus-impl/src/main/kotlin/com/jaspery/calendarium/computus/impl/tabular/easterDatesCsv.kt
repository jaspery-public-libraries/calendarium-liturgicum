/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */
package com.jaspery.calendarium.computus.impl.tabular

import com.opencsv.CSVIterator
import com.opencsv.CSVParserBuilder
import com.opencsv.CSVReader
import com.opencsv.CSVReaderBuilder
import java.io.Reader
import java.net.URL

internal fun parseEasterDatesCsv(url: URL): List<EasterDateFileItem> =
        parseEasterDatesCsv(url.openStream().bufferedReader())

internal fun parseEasterDatesCsv(reader: Reader): List<EasterDateFileItem> {
    return parseCsv(reader) { strings ->
        EasterDateFileItem.parseItem(strings.map { it.trim() })
    }
}

fun <T> parseCsv(reader: Reader, f: (Array<out String>) -> T): List<T> {
    return createCsvReader(reader).use { r ->
        CSVIterator(r).asSequence().map { f(it) }.toList()
    }
}

fun createCsvReader(r: Reader): CSVReader {
    val csvParser = CSVParserBuilder().withSeparator(';').build()
    return CSVReaderBuilder(r).withCSVParser(csvParser).withSkipLines(1).build()
}
