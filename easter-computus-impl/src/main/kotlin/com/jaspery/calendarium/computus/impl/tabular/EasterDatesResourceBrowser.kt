/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.tabular

import java.net.URL

internal object EasterDatesResourceBrowser {
    internal const val EASTER_DATES_RESOURCE = "computus/easter-dates-1951-2050.csv"

    internal val easterDatesResource: URL by lazy {
        checkNotNull(classLoader.getResource(EASTER_DATES_RESOURCE)) {
            "File not found: $EASTER_DATES_RESOURCE. Check if easter-computus-resources is in classpath?"
        }
    }

    internal val classLoader get() = EasterDatesResourceBrowser::class.java.classLoader
}