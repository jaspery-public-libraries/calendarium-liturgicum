/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.tabular

import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import java.net.URL

class EasterDatesResourceBrowserTest {
    private val classLoader: ClassLoader = mockk()

    private val resourceUrl: URL = mockk()

    /**
     * `test resource not found` must run first before lazy property
     * [EasterDatesResourceBrowser.easterDatesResource] is initialized
     */
    @Test
    fun `test resource not found`() {
        every { classLoader.getResource(EasterDatesResourceBrowser.EASTER_DATES_RESOURCE) } returns null

        assertThatThrownBy {
            assertThat(EasterDatesResourceBrowser.easterDatesResource)
        }.isInstanceOf(IllegalStateException::class.java).hasMessageStartingWith("File not found: ")
    }

    @Test(dependsOnMethods = ["test resource not found"])
    fun `test default instance`() {
        every { classLoader.getResource(EasterDatesResourceBrowser.EASTER_DATES_RESOURCE) } returns resourceUrl

        val easterDatesResource: URL = EasterDatesResourceBrowser.easterDatesResource
        assertThat(easterDatesResource).isSameAs(resourceUrl)
    }

    @BeforeMethod
    fun mockEasterDatesResourceBrowser() {
        mockkObject(EasterDatesResourceBrowser)

        every { EasterDatesResourceBrowser.classLoader } returns classLoader
    }

    @AfterMethod
    fun unmockEasterDatesResourceBrowser() {
        verify(exactly = 1) { EasterDatesResourceBrowser.classLoader }
        verify(exactly = 1) { classLoader.getResource(EasterDatesResourceBrowser.EASTER_DATES_RESOURCE) }

        clearMocks(classLoader, resourceUrl)
        unmockkObject(EasterDatesResourceBrowser)
    }
}