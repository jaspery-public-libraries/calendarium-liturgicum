/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.tabular

import com.jaspery.calendarium.computus.EasterComputus
import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.year
import org.assertj.core.api.Assertions
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

class MapBasedEasterComputusTest {
    // we need new instance for each run
    private lateinit var computus: EasterComputus

    @BeforeMethod
    fun setUpComputus() {
        // we need new instance for each run
        computus = MapBasedEasterComputus(listOf(EASTER_2017, EASTER_2018))
    }

    @Test
    fun testEasterComputus_2018() {
        Assertions.assertThat(computus.compute(EASTER_2018.year)).isEqualTo(EASTER_2018.easterDate)

        Assertions.assertThat(computus.computeJulian(EASTER_2018.year)).isEqualTo(EASTER_2018.easterDateJulian)
    }

    @Test
    fun testEasterComputus_2018_errorInFile() {
        val computus = MapBasedEasterComputus(listOf())
        Assertions.assertThatIllegalStateException().isThrownBy {
            computus.compute(EASTER_2018.year)
        }.withMessageStartingWith("Year not found ")

        Assertions.assertThatIllegalStateException().isThrownBy {
            computus.computeJulian(EASTER_2018.year)
        }.withMessageStartingWith("Year not found ")
    }

    @Test
    fun testEasterComputus_1899() {
        Assertions.assertThatIllegalArgumentException().isThrownBy { computus.compute(1899.year) }
        Assertions.assertThatIllegalArgumentException().isThrownBy { computus.computeJulian(1899.year) }
    }

    @Test
    fun testEasterComputus_2101() {
        Assertions.assertThatIllegalArgumentException().isThrownBy { computus.compute(2101.year) }
        Assertions.assertThatIllegalArgumentException().isThrownBy { computus.computeJulian(2101.year) }
    }

    @Test
    fun testEasterComputus_0() {
        Assertions.assertThatIllegalArgumentException().isThrownBy { computus.compute(0.year) }
        Assertions.assertThatIllegalArgumentException().isThrownBy { computus.computeJulian(0.year) }
    }

    companion object {
        private val EASTER_2017 = EasterDateFileItem(2017.year, "2017-04-16".date, "2017-04-16".date)
        private val EASTER_2018 = EasterDateFileItem(2018.year, "2018-04-01".date, "2018-04-08".date)
    }
}