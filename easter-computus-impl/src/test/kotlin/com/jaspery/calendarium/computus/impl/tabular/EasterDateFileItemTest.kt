/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.tabular

import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.year
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatIllegalStateException
import org.testng.annotations.Test

class EasterDateFileItemTest {
    @Test
    fun testParseItem() {
        val item = EasterDateFileItem.parseItem(DATA_1951_LIST)
        assertThat(item).isEqualTo(EASTER_1951)
    }

    @Test
    fun testParseEmpty() {
        assertThatIllegalStateException().isThrownBy {
            EasterDateFileItem.parseItem(emptyList())
        }.withMessageStartingWith("Cannot parse ")
    }

    @Test
    fun testParseTooMuchValues() {
        assertThatIllegalStateException().isThrownBy {
            EasterDateFileItem.parseItem(DATA_1951_INVALID)
        }.withMessageStartingWith("Cannot parse ")
    }

    companion object {
        private val DATA_1951_LIST = listOf("1951", "1951-03-25", "1951-04-29")
        private val DATA_1951_INVALID = listOf("1951", "1951-03-25", "1951-04-29", "")
        private val EASTER_1951 = EasterDateFileItem(1951.year, "1951-03-25".date, "1951-04-29".date)
    }
}