/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.tabular

import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.year
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test
import java.io.ByteArrayInputStream
import java.io.Reader
import java.net.URL

class EasterDatesCsvKtTest {
    @Test
    fun `test create CSV reader`() {
        val reader: Reader = mockk()

        val csvReader = createCsvReader(reader)

        assertThat(csvReader.parser.separator).isEqualTo(';')
        assertThat(csvReader.skipLines).isEqualTo(1)
    }

    @Test
    fun `test parse Easter dates from CSV`() {
        val url: URL = mockk()

        every { url.openStream() } returns ByteArrayInputStream(DATA_SET_VALID.toByteArray(Charsets.UTF_8))

        val result = parseEasterDatesCsv(url)

        assertThat(result).isEqualTo(DATA_LIST_VALID)

        verify(exactly = 1) { url.openStream() }
    }

    companion object {
        private val DATA_SET_VALID = """
            |Year ; Easter Date ; Julian Easter Date
            |1951 ; 1951-03-25  ; 1951-04-29
            |1952 ; 1952-04-13  ; 1952-04-20
        """.trimMargin()

        private val DATA_LIST_VALID = listOf(
                EasterDateFileItem(1951.year, "1951-03-25".date, "1951-04-29".date),
                EasterDateFileItem(1952.year, "1952-04-13".date, "1952-04-20".date)
        )
    }
}
