/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.tabular

import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.year
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import java.net.URL

class MapBasedEasterComputusInstanceTest {
    private val resourceUrl: URL = mockk()

    @Test
    fun `test default instance`() {
        every { EasterDatesResourceBrowser.easterDatesResource } returns resourceUrl
        every { parseEasterDatesCsv(resourceUrl) } returns DATA_LIST_VALID

        val easterDate = MapBasedEasterComputus.instance.compute(1951.year)
        assertThat(easterDate).isEqualTo("1951-03-25".date)

        val easterDateJulian = MapBasedEasterComputus.instance.computeJulian(1952.year)
        assertThat(easterDateJulian).isEqualTo("1952-04-20".date)

        verify { parseEasterDatesCsv(resourceUrl) }
        verify { EasterDatesResourceBrowser.easterDatesResource }
    }

    @BeforeMethod
    fun mockEasterDatesResourceBrowser() = mockkObject(EasterDatesResourceBrowser)

    @AfterMethod
    fun unmockEasterDatesResourceBrowser() = unmockkObject(EasterDatesResourceBrowser)

    @BeforeMethod
    fun mockStaticEasterDatesCsvKt() =
            mockkStatic("com.jaspery.calendarium.computus.impl.tabular.EasterDatesCsvKt")

    @AfterMethod
    fun unmockStaticEasterDatesCsvKt() =
            unmockkStatic("com.jaspery.calendarium.computus.impl.tabular.EasterDatesCsvKt")

    companion object {
        private val DATA_LIST_VALID = listOf(
                EasterDateFileItem(1951.year, "1951-03-25".date, "1951-04-29".date),
                EasterDateFileItem(1952.year, "1952-04-13".date, "1952-04-20".date)
        )
    }
}
