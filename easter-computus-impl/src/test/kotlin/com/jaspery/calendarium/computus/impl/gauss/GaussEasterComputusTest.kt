/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.gauss

import com.jaspery.calendarium.computus.impl.tabular.EasterDateFileItem
import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.year
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.testng.annotations.Test

class GaussEasterComputusTest {
    private val easterComputus: GaussEasterComputus = GaussEasterComputus()

    @Test
    fun `test compute Easter date for 2018 Gregorian`() {
        assertThat(easterComputus.compute(2018.year)).isEqualTo(EASTER_2018.easterDate)
    }

    @Test
    fun `test compute Easter date for 2018 Julian, April branch`() {
        assertThat(easterComputus.computeJulian(2018.year)).isEqualTo(EASTER_2018.easterDateJulian)
    }

    @Test
    fun `test compute Easter date for 2016 Julian, May branch`() {
        assertThat(easterComputus.computeJulian(2016.year)).isEqualTo(EASTER_2016.easterDateJulian)
    }

    @Test
    fun `test Gregorian-Julian conversion`() {
        assertThat(easterComputus.julianCalendarShift(2018)).isEqualTo(13)
    }

    @Test
    fun `test compute Easter date before algorithm valid`() {
        assertThatThrownBy {
            easterComputus.compute(1582.year)
        }.isInstanceOf(IllegalArgumentException::class.java)
                .hasMessageStartingWith("Cannot calculate easter date before 1583, but ")

        assertThatThrownBy {
            easterComputus.computeJulian(1582.year)
        }.isInstanceOf(IllegalArgumentException::class.java)
                .hasMessageStartingWith("Cannot calculate easter date before 1583, but ")
    }

    companion object {
        private val EASTER_2018 = EasterDateFileItem(2018.year, "2018-04-01".date, "2018-04-08".date)
        private val EASTER_2016 = EasterDateFileItem(2016.year, "2016-03-27".date, "2016-05-01".date)
    }
}