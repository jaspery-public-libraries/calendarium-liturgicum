/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.algorithm

import com.jaspery.calendarium.computus.EasterComputus
import com.jaspery.calendarium.computus.impl.gauss.GaussEasterComputus
import com.jaspery.calendarium.computus.impl.tabular.EasterDateFileItem
import com.jaspery.calendarium.computus.impl.tabular.MapBasedEasterComputus
import com.jaspery.kotlin.testng.dataprovider.Scenarios
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import org.assertj.core.api.Assertions.assertThat
import org.testng.ITest
import org.testng.annotations.DataProvider
import org.testng.annotations.Factory
import org.testng.annotations.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.Year

@Test
class EasterComputusImplIntegrationTest
@Factory(dataProvider = "easterComputusAlgorithms")
internal constructor(private val algorithmName: String, private val easterComputus: EasterComputus) : ITest {
    override fun getTestName(): String = "test Easter Computus Algorithm: $algorithmName"

    constructor() : this("It is not a instance of test class but test factory"
            , object : EasterComputus {
        override fun compute(year: Year): LocalDate = TODO("should not be called")
        override fun computeJulian(year: Year): LocalDate = TODO("should not be called")
    })


    @Test(dataProvider = "easterData")
    fun `test Easter Computus Algorithm`(item: EasterDateFileItem) {
        assertThat(easterComputus.compute(item.year))
                .`as`("Testing algorithm $algorithmName for $item.year")
                .isEqualTo(item.easterDate)

        assertThat(easterComputus.computeJulian(item.year))
                .`as`("Testing algorithm $algorithmName (Julian) for $item.year")
                .isEqualTo(item.easterDateJulian)
    }

    @DataProvider
    fun easterComputusAlgorithms() = dataProvider {
        scenario("Tabular", MapBasedEasterComputus.instance)
        scenario("Gauss", GaussEasterComputus())
    }.testNGDataArray()

    @DataProvider
    fun easterData(): Array<Array<Any>> = Scenarios.Builder().apply {
        MapBasedEasterComputus.readEasterDateFile().forEach { it -> this.scenario(it) }
    }.build().testNGDataArray()
}