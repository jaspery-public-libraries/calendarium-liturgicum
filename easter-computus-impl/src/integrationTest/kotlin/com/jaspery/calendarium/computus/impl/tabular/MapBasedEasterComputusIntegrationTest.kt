/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.tabular

import com.jaspery.kotlin.testng.dataprovider.dataProvider
import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.year
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

internal class MapBasedEasterComputusIntegrationTest {
    private val easterComputus = MapBasedEasterComputus.instance

    @Test(dataProvider = "easterComputusDataProvider")
    fun `test MapBasedEasterComputus happy path`(scenario: EasterDateFileItem) {
        assertThat(easterComputus.compute(scenario.year)).isEqualTo(scenario.easterDate)
        assertThat(easterComputus.computeJulian(scenario.year)).isEqualTo(scenario.easterDateJulian)
    }

    @DataProvider
    fun easterComputusDataProvider() = dataProvider {
        scenario(EasterDateFileItem(1951.year, "1951-03-25".date, "1951-04-29".date))
        scenario(EasterDateFileItem(2017.year, "2017-04-16".date, "2017-04-16".date))
        scenario(EasterDateFileItem(2018.year, "2018-04-01".date, "2018-04-08".date))
    }.testNGDataArray()
}