package com.jaspery.calendarium.calendar.season.model

import com.jaspery.threetenbp.ext.LocalDateRange
import org.threeten.bp.Year

class LiturgicalYear(
        val year: Year,
        val range: LocalDateRange
)