/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season

import com.jaspery.calendarium.calendar.season.model.LiturgicalSeason
import com.jaspery.threetenbp.ext.LocalDateRange
import org.threeten.bp.LocalDate
import org.threeten.bp.Year

interface LiturgicalSeasonsService {
    fun getSeasons(year: Year): List<LiturgicalSeason>

    fun getSeasons(year: Year, seasonId: String): List<LiturgicalSeason>

    fun getCurrentSeason(date: LocalDate): LiturgicalSeason

    fun weeksInSeason(season: LiturgicalSeason): Sequence<LocalDateRange>
}