package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.season.model.LiturgicalYear
import com.jaspery.threetenbp.ext.plus
import com.jaspery.threetenbp.ext.year
import org.threeten.bp.LocalDate
import org.threeten.bp.Year

class LiturgicalYearService(
        private val repository: LiturgicalYearRepository
) {

    fun getLiturgicalYear(year: Year): LiturgicalYear = repository.getLiturgicalYear(year)

    fun determineLiturgicalYear(date: LocalDate): LiturgicalYear {
        val thisYearCandidate = repository.getLiturgicalYear(date.year.year)
        if (date in thisYearCandidate.range) {
            return thisYearCandidate
        }
        val nextYearCandidate = repository.getLiturgicalYear(date.year.year + 1)
        if (date in nextYearCandidate.range) {
            return nextYearCandidate
        }
        error("Cannot determine liturgical year for date $date")
    }
}
