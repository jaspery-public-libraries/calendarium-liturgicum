package com.jaspery.calendarium.calendar.dao

import com.jaspery.calendarium.calendar.dao.LiturgicalYearSpecCsvDao.LiturgicalYearSpecDto
import java.net.URL

class LiturgicalYearSpecCsvDao(dataSourceUrl: URL) : BaseCsvDao<LiturgicalYearSpecDto>(dataSourceUrl) {
    public override fun createBeanFromLine(strings: List<String>): LiturgicalYearSpecDto {
        return LiturgicalYearSpecDto(
                strings[0],
                strings[1],
                strings[2],
                strings[3],
                strings[4].toBoolean(),
                strings[5]
        )
    }

    data class LiturgicalYearSpecDto(
            val stDayRange: String,
            val endDayRange: String,
            val startDayRRULE: String,
            val endDayRRULE: String,
            val inPrevYear: Boolean,
            val stSeasonId: String
    )
}
