/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import org.threeten.bp.LocalDate

interface RRuleProcessor {
    fun computeNextDate(startDate: LocalDate, rrule: String, exrrule: String = ""): LocalDate
}
