/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import org.dmfs.rfc5545.recur.InvalidRecurrenceRuleException

/**
 * Parse custom by-easter recurrence rule pattern.
 *
 * Recurrence rule pattern:
 * > FREQ=YEARLY;BYEASTER={int}
 */
class RruleByEasterOffsetParserImpl {
    private val delegate = RruleByDateOffsetParserImpl(PART_BYEASTER)
    internal fun isByEasterRule(rrule: String) = delegate.isByDateRule(rrule)

    /**
     * Parse custom by-easter recurrence rule pattern.
     *
     * @param rrule recurrence rule to parse by-easter offset from. Supported
     * recurrence rule pattern is `FREQ=YEARLY;BYEASTER={int}`
     *
     * @return offset parsed from by-easter recurrence rule.
     *
     * @throws InvalidRecurrenceRuleException if rrule does not conform to
     * supported pattern.
     */
    internal fun parseEasterRruleOffset(rrule: String) = delegate.parseByDateRruleOffset(rrule)

    internal companion object {
        internal const val PART_BYEASTER = "BYEASTER"
    }
}