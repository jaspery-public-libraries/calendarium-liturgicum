/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.dao.LiturgicalHolidaySanctoralSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalHolidaySanctoralSpecCsvDao.LiturgicalHolidaySanctoralSpecDto
import com.jaspery.calendarium.calendar.dao.LiturgicalHolidayTemporalSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalHolidayTemporalSpecCsvDao.LiturgicalHolidayTemporalSpecDto
import com.jaspery.calendarium.calendar.repository.DtoToEntityMapper
import com.jaspery.calendarium.calendar.season.LiturgicalHolidaySpecRepository
import com.jaspery.calendarium.calendar.season.model.LiturgicalDayPrecedence
import com.jaspery.calendarium.calendar.season.model.LiturgicalHolidaySpec
import com.jaspery.calendarium.calendar.season.model.LiturgicalHolidaySpec.SanctoralLiturgicalHolidaySpec
import com.jaspery.calendarium.calendar.season.model.LiturgicalHolidaySpec.TemporalLiturgicalHolidaySpec
import com.jaspery.threetenbp.ext.MonthDayRange
import com.jaspery.threetenbp.ext.parseDayDotMonth
import org.threeten.bp.Month
import org.threeten.bp.MonthDay

class LiturgicalHolidaySpecRepositoryImpl(
        temporalHolidaysDao: LiturgicalHolidayTemporalSpecCsvDao,
        sanctoralHolidaysDao: LiturgicalHolidaySanctoralSpecCsvDao
) : LiturgicalHolidaySpecRepository {
    override val temporalHolidaySpec by lazy { temporalHolidaysDao.getAll().map(temporalHolidayMapper::mapper) }

    override val sanctoralHolidaySpec by lazy { sanctoralHolidaysDao.getAll().map(sanctoralHolidayMapper::mapper) }

    private val temporalHolidaySpecByMonth by lazy {
        temporalHolidaySpec.flatMap {
            monthsRangeAsList(it.dayRange.startInclusive.month, it.dayRange.endInclusive.month)
                    .map { m -> Pair(m, it) }
        }.groupBy { it.first }.mapValues { it.value.map { it.second } }.withDefault { emptyList() }
    }

    private val sanctoralHolidaySpecByDay by lazy { sanctoralHolidaySpec.associateBy { it.date } }

    override fun findHolidaySpecCandidates(monthDay: MonthDay): List<LiturgicalHolidaySpec> {
        val tempHolSpec = temporalHolidaySpecByMonth.getValue(monthDay.month).filter { monthDay in it.dayRange }
        val sanctHolSpec = sanctoralHolidaySpecByDay[monthDay]

        return mutableListOf<LiturgicalHolidaySpec>().apply {
            addAll(tempHolSpec)
            if (sanctHolSpec != null) add(sanctHolSpec)
        }.toList()
    }

    override fun defaultHolidaySpec(monthDay: MonthDay): SanctoralLiturgicalHolidaySpec {
        return SanctoralLiturgicalHolidaySpec(monthDay, DEFAULT_HOLIDAY_ID, DEFAULT_PRECEDENCE, DEFAULT_NAME_EN)
    }

    private val temporalHolidayMapper = TemporalLiturgicalHolidayMapper()

    private val sanctoralHolidayMapper = SanctoralLiturgicalHolidayMapper()

    private class TemporalLiturgicalHolidayMapper : DtoToEntityMapper<TemporalLiturgicalHolidaySpec, LiturgicalHolidayTemporalSpecDto> {
        override fun mapper(from: LiturgicalHolidayTemporalSpecDto) =
                TemporalLiturgicalHolidaySpec(
                        dayRange = MonthDayRange.parse(from.dayRange),
                        nameEn = from.nameEn,
                        seasonId = from.season,
                        ruleEn = from.ruleEn,
                        rank = LiturgicalDayPrecedence.valueOf(from.rankNum, from.rank),
                        holidayId = from.holidayId,
                        rrule = from.rrule,
                        exrrule = from.exrule
                )
    }

    private class SanctoralLiturgicalHolidayMapper : DtoToEntityMapper<SanctoralLiturgicalHolidaySpec, LiturgicalHolidaySanctoralSpecDto> {
        override fun mapper(from: LiturgicalHolidaySanctoralSpecDto): SanctoralLiturgicalHolidaySpec =
                SanctoralLiturgicalHolidaySpec(
                        date = parseDayDotMonth(from.date),
                        holidayId = from.holidayId,
                        rank = LiturgicalDayPrecedence.valueOf(from.rankNum.ifBlank { DEFAULT_RANK_NUM }, from.rank),
                        nameEn = from.nameEn
                )
    }

    private companion object {
        private const val DEFAULT_RANK_ID = "memorial"
        private const val DEFAULT_RANK_NUM = "3.7"
        private const val DEFAULT_HOLIDAY_ID = "feria"
        private const val DEFAULT_NAME_EN = "Ordinary Weekday"
        private val DEFAULT_PRECEDENCE = LiturgicalDayPrecedence.valueOf(DEFAULT_RANK_NUM, DEFAULT_RANK_ID)
    }

    // TODO ihor: move to other module? (maybe not?)
    private fun monthsRangeAsList(start: Month, endIncl: Month): List<Month> = Month.values().filter { m -> m in (start..endIncl) }
}
