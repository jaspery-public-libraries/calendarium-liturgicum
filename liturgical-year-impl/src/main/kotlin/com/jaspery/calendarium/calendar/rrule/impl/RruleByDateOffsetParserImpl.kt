/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import com.jaspery.kotlin.lang.require
import org.dmfs.rfc5545.recur.Freq
import org.dmfs.rfc5545.recur.InvalidRecurrenceRuleException

/**
 * Parse custom by-date recurrence rule pattern.
 *
 * @param byDatePartName name of parameter which defines offset from given date
 *
 * The only supported recurrence rule pattern:
 * > FREQ=YEARLY;<BYDATE>={int}
 */
class RruleByDateOffsetParserImpl(private val byDatePartName: String) {
    init {
        require(byDatePartName, String::isNotBlank)
    }

    internal fun isByDateRule(rrule: String) = rrule.contains(byDatePartName) && rrule.contains(PART_FREQ_YEARLY)

    /**
     * Parse custom by-date recurrence rule pattern.
     *
     * @param rrule recurrence rule to parse by-date offset from. Supported
     * recurrence rule pattern is `FREQ=YEARLY;<[byDatePartName]>={int}`
     *
     * @return offset parsed from by-date recurrence rule.
     *
     * @throws InvalidRecurrenceRuleException if rrule does not conform to
     * supported pattern.
     */
    internal fun parseByDateRruleOffset(rrule: String): Long {
        if (!isByDateRule(rrule)) throwCannotParseException(rrule)

        val kv = rrule.split(';').associate { s ->
            s.splitToSequence('=').map { it.trim() }.zipWithNext().singleOrNull() ?: throwCannotParseException(rrule)
        }

        val freq = kv.getOrDefault(PART_FREQ, "")
        val offset = kv.getOrDefault(byDatePartName, "").toLongOrNull()

        if (kv.size != 2 || freq != Freq.YEARLY.name || offset == null) throwCannotParseException(rrule)

        return offset
    }

    private fun throwCannotParseException(rrule: String): Nothing =
            throw InvalidRecurrenceRuleException("Cannot parse by-date based rule $rrule")

    private companion object {
        private val PART_FREQ = "FREQ" // Cannot use RecurrenceRule.Part.FREQ.name due to unknown error.
        private const val PART_FREQ_YEARLY = "FREQ=YEARLY"
    }
}