/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.resource

import com.jaspery.kotlin.lang.require
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*
import java.util.spi.ResourceBundleControlProvider

class ResourceBundleUtf8ControlProvider : ResourceBundleControlProvider {
    override fun getControl(baseName: String?): ResourceBundle.Control {
        return UTF8Control
    }

    object UTF8Control : ResourceBundle.Control() {
        @Throws(IllegalAccessException::class, InstantiationException::class, IOException::class)
        override fun newBundle(baseName: String, locale: Locale, format: String, loader: ClassLoader, reload: Boolean): ResourceBundle? {
            require(baseName) { it.isNotBlank() }
            requireNotNull(format)
            require(format, { it == FMT_JAVA_CLASS || it == FMT_JAVA_PROPS }) { "Unknown format: $format" }

            if (format == FMT_JAVA_CLASS) {
                return null
            }

            // The below is a copy of the default implementation.
            val resourceName = toResourceName(toBundleName(baseName, locale), "properties")
            val stream: InputStream? =
                    if (reload) {
                        loader.getResource(resourceName)?.openConnection()?.let {
                            it.useCaches = false
                            it.getInputStream()
                        }
                    } else {
                        loader.getResourceAsStream(resourceName)
                    }
            return stream?.use { PropertyResourceBundle(InputStreamReader(it, Charsets.UTF_8)) }
        }
    }

    companion object {
        internal const val FMT_JAVA_CLASS = "java.class"
        internal const val FMT_JAVA_PROPS = "java.properties"
    }
}