package com.jaspery.calendarium.calendar.rrule.impl

import com.jaspery.calendarium.calendar.rrule.RRuleService
import com.jaspery.kotlin.lang.require
import org.threeten.bp.LocalDate

class RRuleServiceImpl(private val rruleProcessorFactory: RRuleProcessorFactory) : RRuleService {
    override fun computeNextDate(startDate: LocalDate, rrule: String, exrrule: String) =
            rruleProcessorFactory.createRRuleProcessor(require(rrule, String::isNotBlank), exrrule)
                    .computeNextDate(startDate, rrule, exrrule)
}