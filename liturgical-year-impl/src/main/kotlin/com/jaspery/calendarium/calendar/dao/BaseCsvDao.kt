package com.jaspery.calendarium.calendar.dao

import com.opencsv.CSVIterator
import com.opencsv.CSVParserBuilder
import com.opencsv.CSVReader
import com.opencsv.CSVReaderBuilder
import java.io.Reader
import java.net.URL

abstract class BaseCsvDao<B : Any>(private val dataSourceUrl: URL) {
    open fun getAll(): List<B> = loadAll()

    protected fun loadAll(): List<B> {
        return parseCsv(dataSourceUrl.openStream().bufferedReader()) { strings ->
            createBeanFromLine(strings.map { it.trim() })
        }
    }

    protected abstract fun createBeanFromLine(strings: List<String>): B

    private fun <T> parseCsv(reader: Reader, f: (Array<out String>) -> T): List<T> {
        return createCsvReader(reader).use { r ->
            CSVIterator(r).asSequence().map { f(it) }.toList()
        }
    }

    private fun createCsvReader(r: Reader): CSVReader {
        val csvParser = CSVParserBuilder().withSeparator(DEFAULT_SEPARATOR).build()
        return CSVReaderBuilder(r).withCSVParser(csvParser).withSkipLines(DEFAULT_SKIP_LINES).build()
    }

    private companion object {
        private const val DEFAULT_SEPARATOR = ';'
        private const val DEFAULT_SKIP_LINES = 1
    }
}