package com.jaspery.calendarium.calendar.repository

interface DtoToEntityMapper<ENT : Any, DTO : Any> {
    fun mapper(from: DTO): ENT
}