/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season

import com.jaspery.calendarium.calendar.season.model.LiturgicalHolidaySpec
import org.threeten.bp.MonthDay

interface LiturgicalHolidaySpecRepository {
    val temporalHolidaySpec: List<LiturgicalHolidaySpec.TemporalLiturgicalHolidaySpec>
    val sanctoralHolidaySpec: List<LiturgicalHolidaySpec.SanctoralLiturgicalHolidaySpec>
    fun findHolidaySpecCandidates(monthDay: MonthDay): List<LiturgicalHolidaySpec>
    fun defaultHolidaySpec(monthDay: MonthDay): LiturgicalHolidaySpec.SanctoralLiturgicalHolidaySpec
}