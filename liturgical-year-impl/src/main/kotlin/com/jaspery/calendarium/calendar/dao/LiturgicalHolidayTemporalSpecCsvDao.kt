package com.jaspery.calendarium.calendar.dao

import com.jaspery.calendarium.calendar.dao.LiturgicalHolidayTemporalSpecCsvDao.LiturgicalHolidayTemporalSpecDto
import java.net.URL

class LiturgicalHolidayTemporalSpecCsvDao(dataSourceUrl: URL) : BaseCsvDao<LiturgicalHolidayTemporalSpecDto>(dataSourceUrl) {
    public override fun createBeanFromLine(strings: List<String>): LiturgicalHolidayTemporalSpecDto {
        return LiturgicalHolidayTemporalSpecDto(
                strings[0],
                strings[1],
                strings[2],
                strings[3],
                strings[4],
                strings[5],
                strings[6],
                strings[7],
                strings[8]
        )
    }

    data class LiturgicalHolidayTemporalSpecDto(
            val dayRange: String,
            val holidayId: String,
            val season: String,
            val rank: String,
            val rankNum: String,
            val nameEn: String,
            val rrule: String,
            val exrule: String,
            val ruleEn: String
    )
}
