package com.jaspery.calendarium.calendar.season.model

import java.util.*

@Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")
class LiturgicalDayPrecedence @ExperimentalUnsignedTypes constructor(
        rankNo: UByte, prec: UByte, rankId: String = DEFAULT_RANK_ID
) : Comparable<LiturgicalDayPrecedence> {
    private val _l1: UByte = rankNo
    private val _l2: UByte = prec

    /** String id for Rank */
    val rankId: String = rankId
    /** Precedence of Rank */
    val rankNo: UByte get() = _l1
    /** Precedence inside Rank*/
    val prec: UByte get() = _l2

    override fun compareTo(other: LiturgicalDayPrecedence): Int {
        return when (val r = _l1 - other._l1) {
            0u -> _l2 - other._l2
            else -> r
        }.toInt()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LiturgicalDayPrecedence) return false
        return _l1 == other._l1 && _l2 == other._l2
    }

    override fun hashCode(): Int = Objects.hash(_l1, _l2)

    override fun toString() = "$_l1.$_l2"

    private constructor(rankNum: Pair<UByte, UByte>, rankId: String) : this(rankNum.first, rankNum.second, rankId)

    companion object {
        private const val DEFAULT_RANK_ID = "default"
        private const val ERROR_MESSAGE = "Cannot parse \"%s\" as ParagraphNumber"

        fun valueOf(rankNum: String, rankId: String = DEFAULT_RANK_ID) = LiturgicalDayPrecedence(parse(rankNum), rankId)

        internal fun parse(paragraphNumber: String): Pair<UByte, UByte> = try {
            paragraphNumber.split('.').map { it.toUByte() }.let {
                if (it.size != 2) {
                    throw IllegalArgumentException(ERROR_MESSAGE.format(paragraphNumber))
                }
                Pair(it[0], it[1])
            }
        } catch (e: NumberFormatException) {
            throw IllegalArgumentException(ERROR_MESSAGE.format(paragraphNumber), e)
        }
    }
}