/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season.model

import com.jaspery.threetenbp.ext.MonthDayRange

data class LiturgicalSeasonSpec(
        val startDayRange: MonthDayRange,
        val endDayRange: MonthDayRange,
        val seasonId: String,
        val primaryColor: String,
        val nameEn: String,
        val startsInPreviousYear: Boolean,
        val startDayRrule: String,
        val endDayRrule: String,
        val startsWithVesper: Boolean,
        val endsBeforeVesper: Boolean
)