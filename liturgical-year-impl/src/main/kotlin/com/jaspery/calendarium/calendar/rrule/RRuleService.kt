package com.jaspery.calendarium.calendar.rrule

import org.threeten.bp.LocalDate

interface RRuleService {
    fun computeNextDate(startDate: LocalDate, rrule: String, exrrule: String = ""): LocalDate
}