/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.resource

import java.net.URL
import java.util.*

/**
 * ```
 *  calendarium
 *  |- roman
 *      |- general
 *      |    |- colors.yaml
 *      |    |- temporal-seasons.yaml
 *      |    |- temporal-holidays.yaml
 *      |    |- sanctoral-holidays.yaml
 *      |    |- temporal-seasons.properties
 *      |    |- temporal-seasons_en.properties
 *      |    |- temporal-seasons_uk.properties
 *      |- variants
 *      |    |- ukraine
 *      |    |    |- sanctoral-holidays.yaml
 *      |    |- index.yaml (list all variants/sub-directories)
 *      |- families
 *      |    |- dominican-order
 *      |    |    |- sanctoral-holidays.yaml
 *      |    |- index.yaml (list all families/sub-directories)
 *
 * ```
 */
@Suppress("unused")
interface CalendarResourceBrowser {
    fun calendarVariants(rite: Rite): List<String> = TODO("not implemented")
    fun calendarFamilies(rite: Rite): List<String> = TODO("not implemented")

    fun generalCalendarResource(rite: Rite, fileName: CalendarFileName): URL
    fun calendarVariantResources(rite: Rite): Map<CalendarFileName, URL> = TODO("not implemented")
    fun calendarFamilyResources(rite: Rite): Map<CalendarFileName, URL> = TODO("not implemented")

    fun generalCalendarResourceBundle(rite: Rite, fileName: CalendarFileName, locale: Locale = Locale.getDefault()): ResourceBundle
    fun calendarVariantResourceBundle(rite: Rite, variantId: String, fileName: CalendarFileName): ResourceBundle = TODO("not implemented")
    fun calendarFamilyResourceBundle(rite: Rite, familyId: String, fileName: CalendarFileName): ResourceBundle = TODO("not implemented")
    fun calendarVariantNamesResourceBundle(rite: Rite): ResourceBundle = TODO("not implemented")
    fun calendarFamilyNamesResourceBundle(rite: Rite): ResourceBundle = TODO("not implemented")

    enum class Rite(val riteId: String) {
        ROMAN("roman"), EASTERN("eastern")
    }

    enum class CalendarFileName(val fileName: String) {
        COLORS("colors"),
        LITURGICAL_YEAR("liturgical-year"),
        TEMPORAL_SEASONS("temporal-seasons"),
        TEMPORAL_HOLIDAYS("temporal-holidays"),
        SANCTORAL_HOLIDAYS("sanctoral-holidays");

        fun baseName(vararg pathComponents: String) =
                pathComponents.fold("") { acc, v -> "$acc$v." } + fileName

        fun path(vararg pathComponents: String) =
                pathComponents.fold("") { acc, v -> "$acc$v/" } + "$fileName$FILE_EXT"
    }

    companion object {
        private const val FILE_INDEX = "index.yaml"
        private const val FILE_EXT = ".csv"

        internal const val DIR_ROOT = "calendarium"

        internal const val DIR_GENERAL = "general"

        private val DIR_ROMAN_RITE = "$DIR_ROOT/${Rite.ROMAN.riteId}"
        private val DIR_ROMAN_GENERAL = "$DIR_ROMAN_RITE/general"
        private val DIR_ROMAN_VARIANTS = "$DIR_ROMAN_RITE/variants"
        private val DIR_ROMAN_FAMILIES = "$DIR_ROMAN_RITE/families"

        private val GEN_ROM_CAL_INDEX = "$DIR_ROMAN_GENERAL/$FILE_INDEX"
        private val ROMAN_VARIANTS_INDEX = "$DIR_ROMAN_VARIANTS/$FILE_INDEX"

        private val DIR_EASTERN_RITE = "$DIR_ROOT/${Rite.EASTERN.riteId}"
        private val DIR_EASTERN_GENERAL = "$DIR_EASTERN_RITE/general"
        private val DIR_EASTERN_VARIANTS = "$DIR_EASTERN_RITE/variants"
        private val DIR_EASTERN_FAMILIES = "$DIR_EASTERN_RITE/families"
    }
}
