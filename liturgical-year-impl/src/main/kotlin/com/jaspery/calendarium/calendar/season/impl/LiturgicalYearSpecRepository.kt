/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.dao.LiturgicalYearSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalYearSpecCsvDao.LiturgicalYearSpecDto
import com.jaspery.calendarium.calendar.repository.DtoToEntityMapper
import com.jaspery.calendarium.calendar.season.model.LiturgicalYearSpec
import com.jaspery.threetenbp.ext.MonthDayRange

class LiturgicalYearSpecRepository(dao: LiturgicalYearSpecCsvDao) {
    private val mapper = LiturgicalYearSpecMapper()

    val liturgicalYearSpec by lazy { dao.getAll().single().let { mapper.mapper(it) } }

    private class LiturgicalYearSpecMapper : DtoToEntityMapper<LiturgicalYearSpec, LiturgicalYearSpecDto> {
        override fun mapper(from: LiturgicalYearSpecDto): LiturgicalYearSpec = LiturgicalYearSpec(
                startDayRange = MonthDayRange.parse(from.stDayRange),
                endDayRange = MonthDayRange.parse(from.endDayRange),
                startDayRrule = from.startDayRRULE,
                endDayRrule = from.endDayRRULE,
                startSeasonId = from.stSeasonId,
                startsInPreviousYear = from.inPrevYear
        )
    }
}
