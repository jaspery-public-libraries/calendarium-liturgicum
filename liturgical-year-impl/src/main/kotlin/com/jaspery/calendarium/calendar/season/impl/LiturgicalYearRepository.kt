/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.rrule.RRuleService
import com.jaspery.calendarium.calendar.season.model.LiturgicalYear
import com.jaspery.kotlin.justcache.Cache
import com.jaspery.kotlin.justcache.CacheConfig
import com.jaspery.kotlin.justcache.CacheConfig.CacheCapacity
import com.jaspery.kotlin.justcache.CacheConfig.ReplacementPolicy
import com.jaspery.kotlin.justcache.JustCacheBuilder
import com.jaspery.kotlin.justcache.JustCacheBuilder.CacheLoadType
import com.jaspery.threetenbp.ext.LocalDateRange
import com.jaspery.threetenbp.ext.atYear
import com.jaspery.threetenbp.ext.minus
import org.threeten.bp.LocalDate
import org.threeten.bp.Month
import org.threeten.bp.MonthDay
import org.threeten.bp.Year

class LiturgicalYearRepository(
        private val specRepository: LiturgicalYearSpecRepository,
        private val rruleService: RRuleService
) {
    fun getLiturgicalYear(year: Year): LiturgicalYear = liturgicalYears[year]

    private val liturgicalYears: Cache<Year, LiturgicalYear> =
            JustCacheBuilder.build {
                loadType(CacheLoadType.ReadThrough { LiturgicalYear(it, liturgicalYearDateRange(it)) })
                capacity(CacheCapacity.Capped(MAX_CACHE_SIZE))
                replacementPolicy(ReplacementPolicy.LRU)
                evictionType(CacheConfig.EvictionType.EvictBatch(MAX_CACHE_SIZE))
            }

    private fun liturgicalYearDateRange(year: Year): LocalDateRange {
        val yearSpec = specRepository.liturgicalYearSpec
        val stDtStartWith = if (yearSpec.startsInPreviousYear) previousYearStDate(year) else currentYearStDate(year)
        val enDtStartWith = currentYearStDate(year)
        return LocalDateRange(
                startInclusive = rruleService.computeNextDate(stDtStartWith, yearSpec.startDayRrule),
                endInclusive = rruleService.computeNextDate(enDtStartWith, yearSpec.endDayRrule)
        )
    }

    private fun previousYearStDate(year: Year): LocalDate = PREV_YEAR_RRULE_ST_DATE.atYear(year - 1)

    private fun currentYearStDate(year: Year): LocalDate = CURR_YEAR_RRULE_ST_DATE.atYear(year)

    private companion object {
        private const val MAX_CACHE_SIZE = 5
        private val PREV_YEAR_RRULE_ST_DATE = MonthDay.of(Month.SEPTEMBER, 1)
        private val CURR_YEAR_RRULE_ST_DATE = MonthDay.of(Month.JANUARY, 1)
    }
}
