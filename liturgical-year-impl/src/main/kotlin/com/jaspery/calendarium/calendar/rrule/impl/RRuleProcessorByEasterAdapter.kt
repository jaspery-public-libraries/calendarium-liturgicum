/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import com.jaspery.calendarium.computus.EasterComputus
import com.jaspery.kotlin.lang.require
import com.jaspery.threetenbp.ext.plus
import com.jaspery.threetenbp.ext.year
import org.threeten.bp.LocalDate

class RRuleProcessorByEasterAdapter(
        private val easterComputus: EasterComputus,
        private val rruleByEasterOffsetParser: RruleByEasterOffsetParserImpl) : RRuleProcessor {
    override fun computeNextDate(startDate: LocalDate, rrule: String, exrrule: String): LocalDate {
        require(exrrule, String::isBlank) {
            "EXRULE is not supported for BYEASTER based recurrence rules: RRULE = $rrule, EXRULE=$exrrule"
        }
        val byEasterOffset = rruleByEasterOffsetParser.parseEasterRruleOffset(rrule)
        val candidate = easterComputus.compute(startDate.year.year).plusDays(byEasterOffset)
        return if (candidate >= startDate) {
            candidate
        } else {
            easterComputus.compute(startDate.year.year + 1).plusDays(byEasterOffset)
        }
    }
}
