package com.jaspery.calendarium.calendar.season

import com.jaspery.calendarium.calendar.season.model.LiturgicalHoliday
import org.threeten.bp.LocalDate

interface LiturgicalHolidaysService {
    fun getLiturgicalHoliday(date: LocalDate): LiturgicalHoliday
}