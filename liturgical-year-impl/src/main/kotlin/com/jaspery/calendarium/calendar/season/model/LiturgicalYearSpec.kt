/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season.model

import com.jaspery.threetenbp.ext.MonthDayRange

data class LiturgicalYearSpec(
        val startDayRange: MonthDayRange,
        val endDayRange: MonthDayRange,
        val startDayRrule: String,
        val endDayRrule: String,
        val startsInPreviousYear: Boolean,
        val startSeasonId: String
)