/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.resource

import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.CalendarFileName
import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.Companion.DIR_GENERAL
import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.Companion.DIR_ROOT
import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.Rite
import java.net.URL
import java.util.*

class ClasspathCalendarResourceBrowser(
        private val classLoader: ClassLoader,
        private val resourceBundleControl: ResourceBundle.Control? = null)
    : CalendarResourceBrowser {

    override fun generalCalendarResource(rite: Rite, fileName: CalendarFileName): URL =
            generalCalendarResources(rite)[fileName] ?: classpathResourceNotFoundError(rite, fileName)

    private fun generalCalendarResources(rite: Rite): Map<CalendarFileName, URL> =
            CalendarFileName.values().associate {
                it to classLoader.getResource(it.path(DIR_ROOT, rite.riteId, "general"))
            }

    override fun generalCalendarResourceBundle(rite: Rite, fileName: CalendarFileName, locale: Locale): ResourceBundle {
        return getResourceBundle(fileName.baseName(DIR_ROOT, rite.riteId, DIR_GENERAL), locale, resourceBundleControl)
    }

    private fun classpathResourceNotFoundError(rite: Rite, fileName: CalendarFileName): Nothing =
            error("Classpath resource $fileName not found for rite $rite")

    /**
     * Semantic of this function differs from [ResourceBundle.getBundle]: if control is null it
     * uses java default control implementation.
     */
    private fun getResourceBundle(baseName: String, targetLocale: Locale, control: ResourceBundle.Control? = null): ResourceBundle {
        return if (control == null) {
            ResourceBundle.getBundle(baseName, targetLocale)
        } else {
            ResourceBundle.getBundle(baseName, targetLocale, control)
        }
    }
}