/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import com.jaspery.calendarium.computus.EasterComputus
import com.jaspery.kotlin.lang.require

class RRuleProcessorFactory(
        private val easterComputus: EasterComputus,
        private val rruleByEasterOffsetParser: RruleByEasterOffsetParserImpl = RruleByEasterOffsetParserImpl()
) {
    fun createRRuleProcessor(rrule: String, exrrule: String = "") =
            if (rruleByEasterOffsetParser.isByEasterRule(rrule)) {
                require(exrrule, String::isBlank) { "Exception rules are not supported in conjuctions with by-date rules" }
                RRuleProcessorByEasterAdapter(easterComputus, rruleByEasterOffsetParser)
            } else if (rrule.isNotBlank() && exrrule.isNotBlank()) {
                RRuleSetProcessorDmfsAdaptor()
            } else {
                RRuleProcessorDmfsAdaptor()
            }
}