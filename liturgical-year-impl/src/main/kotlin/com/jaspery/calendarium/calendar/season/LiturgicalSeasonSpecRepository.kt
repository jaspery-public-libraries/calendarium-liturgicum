/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season

import com.jaspery.calendarium.calendar.season.model.LiturgicalSeasonSpec
import org.threeten.bp.MonthDay

interface LiturgicalSeasonSpecRepository {
    val liturgicalSeasonsSpec: List<LiturgicalSeasonSpec>
    val liturgicalSeasonSpecById: Map<String, LiturgicalSeasonSpec>
    fun findSeasonSpecCandidates(monthDay: MonthDay): List<LiturgicalSeasonSpec>
}