package com.jaspery.calendarium.calendar.dao

import com.jaspery.calendarium.calendar.dao.LiturgicalSeasonSpecCsvDao.LiturgicalSeasonSpecDto
import java.net.URL

class LiturgicalSeasonSpecCsvDao(dataSourceUrl: URL) : BaseCsvDao<LiturgicalSeasonSpecDto>(dataSourceUrl) {
    public override fun createBeanFromLine(strings: List<String>): LiturgicalSeasonSpecDto {
        return LiturgicalSeasonSpecDto(
                strings[0],
                strings[1],
                strings[2],
                strings[3],
                strings[4],
                strings[5].toBoolean(),
                strings[6],
                strings[7],
                strings[8].toBoolean(),
                strings[9].toBoolean()
        )
    }

    data class LiturgicalSeasonSpecDto(
            val stDayRange: String,
            val endDayRange: String,
            val seasonId: String,
            val primColor: String,
            val nameEn: String,
            val inPrevYear: Boolean,
            val startDayRRULE: String,
            val endDayRRULE: String,
            val stWVesp: Boolean,
            val enBfVesp: Boolean
    )
}