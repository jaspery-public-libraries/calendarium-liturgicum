/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.season.LiturgicalSeasonSpecRepository
import com.jaspery.calendarium.calendar.season.LiturgicalSeasonsService
import com.jaspery.calendarium.calendar.season.model.LiturgicalSeason
import com.jaspery.threetenbp.ext.LocalDateRange
import com.jaspery.threetenbp.ext.monthDay
import org.threeten.bp.LocalDate
import org.threeten.bp.Year

class LiturgicalSeasonsServiceImpl(
        private val specRepository: LiturgicalSeasonSpecRepository,
        private val repository: LiturgicalSeasonRepository,
        private val litYearService: LiturgicalYearService
) : LiturgicalSeasonsService {
    override fun getSeasons(year: Year): List<LiturgicalSeason> = repository.getSeasons(litYearService.getLiturgicalYear(year))

    override fun getSeasons(year: Year, seasonId: String) = getSeasons(year).filter { it.seasonId == seasonId }

    override fun getCurrentSeason(date: LocalDate): LiturgicalSeason {
        val candidateSpecs = specRepository.findSeasonSpecCandidates(date.monthDay)
        val litYear = litYearService.determineLiturgicalYear(date)
        val candidates = candidateSpecs.map { repository.getLiturgicalSeason(litYear, it) }
        return candidates.single { date in it.dateRange }
    }

    override fun weeksInSeason(season: LiturgicalSeason): Sequence<LocalDateRange> {
        return getSeasons(season.liturgicalYear, season.seasonId).map { it.weeks() }.reduce { a, b -> a + b }
    }
}
