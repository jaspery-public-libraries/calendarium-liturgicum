package com.jaspery.calendarium.calendar.season.model

import com.jaspery.threetenbp.ext.MonthDayRange
import org.threeten.bp.MonthDay

sealed class LiturgicalHolidaySpec {
    abstract val holidayId: String
    abstract val rank: LiturgicalDayPrecedence
    abstract val nameEn: String

    data class SanctoralLiturgicalHolidaySpec(
            val date: MonthDay,
            override val holidayId: String,
            override val rank: LiturgicalDayPrecedence,
            override val nameEn: String
    ) : LiturgicalHolidaySpec()

    data class TemporalLiturgicalHolidaySpec(
            val dayRange: MonthDayRange,
            override val holidayId: String,
            override val rank: LiturgicalDayPrecedence,
            override val nameEn: String,
            val seasonId: String,
            val rrule: String,
            val exrrule: String,
            val ruleEn: String
    ) : LiturgicalHolidaySpec()
}
