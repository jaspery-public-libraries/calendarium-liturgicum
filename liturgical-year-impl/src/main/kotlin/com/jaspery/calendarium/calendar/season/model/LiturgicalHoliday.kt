package com.jaspery.calendarium.calendar.season.model

import org.threeten.bp.LocalDate

data class LiturgicalHoliday(
        val date: LocalDate,
        val holidayId: String,
        val seasonId: String,
        val weekNo: Int,
        val precedence: LiturgicalDayPrecedence
)