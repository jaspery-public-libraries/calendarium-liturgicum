/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.dao.LiturgicalSeasonSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalSeasonSpecCsvDao.LiturgicalSeasonSpecDto
import com.jaspery.calendarium.calendar.repository.DtoToEntityMapper
import com.jaspery.calendarium.calendar.season.LiturgicalSeasonSpecRepository
import com.jaspery.calendarium.calendar.season.model.LiturgicalSeasonSpec
import com.jaspery.threetenbp.ext.MonthDayRange
import com.jaspery.threetenbp.ext.monthDay
import org.threeten.bp.MonthDay

class LiturgicalSeasonSpecRepositoryImpl(dao: LiturgicalSeasonSpecCsvDao) : LiturgicalSeasonSpecRepository {
    private val mapper = LiturgicalSeasonSpecMapper()

    override val liturgicalSeasonsSpec by lazy { dao.getAll().map(mapper::mapper) }

    override val liturgicalSeasonSpecById by lazy { liturgicalSeasonsSpec.associateBy { it.seasonId } }

    override fun findSeasonSpecCandidates(monthDay: MonthDay): List<LiturgicalSeasonSpec> =
            liturgicalSeasonsSpec.filter {
                val stDay = it.startDayRange.startInclusive
                val enDay = it.endDayRange.endInclusive
                // case when range spans end of year we have to split it in two ranges
                if (stDay <= enDay) monthDay in stDay..enDay
                else (monthDay in stDay..YEAR_EN_MD || monthDay in YEAR_ST_MD..enDay)
            }

    private class LiturgicalSeasonSpecMapper : DtoToEntityMapper<LiturgicalSeasonSpec, LiturgicalSeasonSpecDto> {
        override fun mapper(from: LiturgicalSeasonSpecDto): LiturgicalSeasonSpec = LiturgicalSeasonSpec(
                startDayRange = MonthDayRange.parse(from.stDayRange),
                endDayRange = MonthDayRange.parse(from.endDayRange),
                seasonId = from.seasonId,
                primaryColor = from.primColor,
                nameEn = from.nameEn,
                startsInPreviousYear = from.inPrevYear,
                startDayRrule = from.startDayRRULE,
                endDayRrule = from.endDayRRULE,
                startsWithVesper = from.stWVesp,
                endsBeforeVesper = from.enBfVesp
        )
    }

    private companion object {
        // TODO ihor: move to another class? Not-yet - used only here
        private val YEAR_ST_MD = "01.01".monthDay
        // TODO ihor: move to another class? Not-yet - used only here
        private val YEAR_EN_MD = "31.12".monthDay
    }
}
