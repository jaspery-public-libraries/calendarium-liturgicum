/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import org.dmfs.rfc5545.DateTime
import org.dmfs.rfc5545.recur.RecurrenceRule
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId


/**
 * Construct [DateTime] from [LocalDate] instance.
 *
 * *Note* month is 0-based in [DateTime].
 *
 * @return [DateTime] instance corresponding to [this] LocalDate instance
 */
fun LocalDate.toDateTime() = DateTime(this.year, this.monthValue - 1, this.dayOfMonth)

/**
 * Convert [DateTime] to [LocalDate] in time-zone [zoneId].
 *
 * @param zoneId 3.10bp [ZoneId] to use for conversion
 * @return [LocalDate] instance corresponding to [this] [DateTime] instance
 */
fun DateTime.toLocalDate(zoneId: ZoneId): LocalDate =
        Instant.ofEpochMilli(this.timestamp).atZone(zoneId).toLocalDate()

/**
 * Wraps call to [RecurrenceRule.iterator().nextDateTime()] to make method call
 * more concise. Adapts dmfs date-time framework classes to threetenbp date-time
 * classes.
 *
 * @param startDate
 */
fun RecurrenceRule.nextDateTime(startDate: LocalDate, zoneId: ZoneId): LocalDate =
        this.iterator(startDate.toDateTime()).nextDateTime().toLocalDate(zoneId)