/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import com.jaspery.kotlin.lang.require
import org.dmfs.rfc5545.recur.RecurrenceRule
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId

class RRuleProcessorDmfsAdaptor(
        private val recurrenceRuleCreator: (String) -> RecurrenceRule = defaultRecurrenceRuleCreator
) : RRuleProcessor {
    override fun computeNextDate(startDate: LocalDate, rrule: String, exrrule: String): LocalDate {
        require(exrrule, String::isBlank)
        return recurrenceRuleCreator(rrule).nextDateTime(startDate, ZoneId.systemDefault())
    }

    companion object {
        internal val defaultRecurrenceRuleCreator
                : (String) -> RecurrenceRule = { rrule -> RecurrenceRule(rrule, RecurrenceRule.RfcMode.RFC5545_STRICT) }
    }
}