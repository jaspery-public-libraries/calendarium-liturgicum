/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season.model

import com.jaspery.threetenbp.ext.LocalDateRange
import org.threeten.bp.DayOfWeek
import org.threeten.bp.DayOfWeek.SATURDAY
import org.threeten.bp.DayOfWeek.SUNDAY
import org.threeten.bp.Year
import org.threeten.bp.temporal.TemporalAdjusters
import org.threeten.bp.temporal.TemporalAdjusters.next

data class LiturgicalSeason(
        val dateRange: LocalDateRange,
        val seasonId: String,
        val nameEn: String,
        val liturgicalYear: Year,
        val startWithVesper: Boolean,
        val endBeforeVesper: Boolean
) {
    /**
     * Returns weeks only in part of season represented by this object. E.g. Ordinary Time season in
     * Roman rite is split into two parts.
     *
     * @return weeks represented by [LocalDateRange]s
     */
    fun weeks(): Sequence<LocalDateRange> {
        val startDayOfWeek = dateRange.startInclusive.dayOfWeek
        return dateRange.daysOfWeekSequence(startDayOfWeek, SUNDAY).map { LocalDateRange(it, it.with(next(SATURDAY))) }
    }

    private fun LocalDateRange.daysOfWeekSequence(firstDayOfWeek: DayOfWeek, otherDaysOfWeek: DayOfWeek) =
            generateSequence(startInclusive.with(TemporalAdjusters.nextOrSame(firstDayOfWeek))) { it.with(TemporalAdjusters.next(otherDaysOfWeek)) }
                    .takeWhile { it <= endInclusive }
}