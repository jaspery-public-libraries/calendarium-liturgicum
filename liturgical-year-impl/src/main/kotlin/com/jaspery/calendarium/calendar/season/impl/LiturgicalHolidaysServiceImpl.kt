package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.rrule.RRuleService
import com.jaspery.calendarium.calendar.season.LiturgicalHolidaySpecRepository
import com.jaspery.calendarium.calendar.season.LiturgicalHolidaysService
import com.jaspery.calendarium.calendar.season.LiturgicalSeasonsService
import com.jaspery.calendarium.calendar.season.model.LiturgicalHoliday
import com.jaspery.calendarium.calendar.season.model.LiturgicalHolidaySpec
import com.jaspery.calendarium.calendar.season.model.LiturgicalHolidaySpec.SanctoralLiturgicalHolidaySpec
import com.jaspery.calendarium.calendar.season.model.LiturgicalHolidaySpec.TemporalLiturgicalHolidaySpec
import com.jaspery.threetenbp.ext.atYear
import com.jaspery.threetenbp.ext.monthDay
import com.jaspery.threetenbp.ext.year
import org.threeten.bp.LocalDate
import org.threeten.bp.Year

class LiturgicalHolidaysServiceImpl(
        private val liturgicalSeasonsService: LiturgicalSeasonsService,
        private val repository: LiturgicalHolidaySpecRepository,
        private val rruleService: RRuleService
) : LiturgicalHolidaysService {
    override fun getLiturgicalHoliday(date: LocalDate): LiturgicalHoliday {
        val monthDay = date.monthDay
        val daySpecs = repository.findHolidaySpecCandidates(monthDay)
        val season = liturgicalSeasonsService.getCurrentSeason(date)
        val weeks = liturgicalSeasonsService.weeksInSeason(season).takeWhile { it.startInclusive <= date }
        val days = daySpecs
                .map { convertSpecToDay(it, season.seasonId, weeks.count(), date.year.year) }
        val holiday = days.firstOrNull { it.date == date }
        return holiday
                ?: convertSpecToDay(repository.defaultHolidaySpec(monthDay), season.seasonId, weeks.count(), date.year.year)
    }

    private fun calculateDate(spec: LiturgicalHolidaySpec, year: Year) = when (spec) {
        is SanctoralLiturgicalHolidaySpec -> spec.date.atYear(year)
        is TemporalLiturgicalHolidaySpec -> rruleService.computeNextDate(spec.calcStartDate(year), spec.rrule, spec.exrrule)
    }

    private fun convertSpecToDay(spec: LiturgicalHolidaySpec, seasonId: String, seasonWeekNo: Int, year: Year) =
            LiturgicalHoliday(calculateDate(spec, year), spec.holidayId, seasonId, seasonWeekNo, spec.rank)

    private fun TemporalLiturgicalHolidaySpec.calcStartDate(year: Year): LocalDate =
            dayRange.startInclusive.atYear(year).minusDays(1)
}
