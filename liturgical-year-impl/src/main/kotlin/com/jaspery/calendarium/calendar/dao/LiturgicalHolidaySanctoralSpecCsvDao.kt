package com.jaspery.calendarium.calendar.dao

import com.jaspery.calendarium.calendar.dao.LiturgicalHolidaySanctoralSpecCsvDao.LiturgicalHolidaySanctoralSpecDto
import java.net.URL

class LiturgicalHolidaySanctoralSpecCsvDao(dataSourceUrl: URL) : BaseCsvDao<LiturgicalHolidaySanctoralSpecDto>(dataSourceUrl) {
    public override fun createBeanFromLine(strings: List<String>): LiturgicalHolidaySanctoralSpecDto {
        return LiturgicalHolidaySanctoralSpecDto(
                strings[0],
                strings[1],
                strings[2],
                strings[3],
                strings[4]
        )
    }

    data class LiturgicalHolidaySanctoralSpecDto(
            val date: String,
            val holidayId: String,
            val rank: String,
            val rankNum: String,
            val nameEn: String
    )
}