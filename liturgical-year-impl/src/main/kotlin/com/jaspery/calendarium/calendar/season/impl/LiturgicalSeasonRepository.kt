package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.rrule.RRuleService
import com.jaspery.calendarium.calendar.season.LiturgicalSeasonSpecRepository
import com.jaspery.calendarium.calendar.season.model.LiturgicalSeason
import com.jaspery.calendarium.calendar.season.model.LiturgicalSeasonSpec
import com.jaspery.calendarium.calendar.season.model.LiturgicalYear
import com.jaspery.kotlin.justcache.Cache
import com.jaspery.kotlin.justcache.CacheConfig.*
import com.jaspery.kotlin.justcache.JustCacheBuilder
import com.jaspery.kotlin.justcache.JustCacheBuilder.CacheLoadType.ReadThrough
import com.jaspery.threetenbp.ext.LocalDateRange
import com.jaspery.threetenbp.ext.monthDay
import java.util.*

class LiturgicalSeasonRepository(
        private val specRepository: LiturgicalSeasonSpecRepository,
        private val rruleService: RRuleService
) {
    private val seasonCache: Cache<SeasonSpecYearKey, LiturgicalSeason> = JustCacheBuilder.build {
        loadType(ReadThrough { convertSpecToSeason(it.spec, it.year) })
        capacity(CacheCapacity.Capped(MAX_CACHE_SIZE))
        replacementPolicy(ReplacementPolicy.LRU)
        evictionType(EvictionType.EvictBatch(CACHE_CLEAR_CHUNK_SIZE))
    }

    fun getLiturgicalSeason(y: LiturgicalYear, s: LiturgicalSeasonSpec) = seasonCache[SeasonSpecYearKey(y, s)]

    fun getSeasons(litYear: LiturgicalYear): List<LiturgicalSeason> =
            litYear.let { y ->
                specRepository.liturgicalSeasonsSpec.map { s ->
                    getLiturgicalSeason(y, s)
                }
            }

    private fun convertSpecToSeason(spec: LiturgicalSeasonSpec, litYear: LiturgicalYear): LiturgicalSeason =
            LiturgicalSeason(
                    computeSeasonDateRange(litYear, spec).also { postCalculationCheck(it, spec, litYear) },
                    spec.seasonId, spec.nameEn, litYear.year, spec.startsWithVesper, spec.endsBeforeVesper
            )

    private fun computeSeasonDateRange(litYear: LiturgicalYear, it: LiturgicalSeasonSpec) = LocalDateRange(
            startInclusive = rruleService.computeNextDate(litYear.range.startInclusive, it.startDayRrule),
            endInclusive = rruleService.computeNextDate(litYear.range.startInclusive, it.endDayRrule)
    )

    private fun postCalculationCheck(seasonDateRange: LocalDateRange, spec: LiturgicalSeasonSpec, litYear: LiturgicalYear) {
        check(seasonDateRange.startInclusive.monthDay in spec.startDayRange) {
            "Season ${spec.seasonId} start date ${seasonDateRange.startInclusive} not in expected range ${spec.startDayRange}"
        }
        check(seasonDateRange.endInclusive.monthDay in spec.endDayRange) {
            "Season ${spec.seasonId} end date ${seasonDateRange.endInclusive} not in expected range ${spec.endDayRange}"
        }
        check(seasonDateRange.startInclusive in litYear.range) {
            "Season ${spec.seasonId} start date ${seasonDateRange.startInclusive} not in expected range ${litYear.range}"
        }
        check(seasonDateRange.endInclusive in litYear.range) {
            "Season ${spec.seasonId} end date ${seasonDateRange.endInclusive} not in expected range ${litYear.range}"
        }
    }

    private class SeasonSpecYearKey(
            internal val year: LiturgicalYear,
            internal val spec: LiturgicalSeasonSpec
    ) {
        private val hashCodeCache by lazy {
            Objects.hash(year.year, spec.seasonId, spec.startDayRrule)
        }

        override fun hashCode(): Int = hashCodeCache

        override fun equals(other: Any?): Boolean {
            if (this === other) {
                return true
            }
            if (other !is SeasonSpecYearKey) {
                return false
            }
            return year == other.year && spec.seasonId == other.spec.seasonId &&
                    spec.startDayRrule == other.spec.startDayRrule
        }

        override fun toString(): String {
            return "Liturgical Season Key(year: ${year.year}, seasonId: ${spec.seasonId}, rrule: ${spec.startDayRrule})"
        }
    }

    private companion object {
        private const val MAX_CACHE_SIZE = 21
        private const val CACHE_CLEAR_CHUNK_SIZE = 7
    }
}
