/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import com.jaspery.kotlin.lang.require
import org.dmfs.rfc5545.DateTime
import org.dmfs.rfc5545.recur.RecurrenceRule
import org.dmfs.rfc5545.recurrenceset.RecurrenceList
import org.dmfs.rfc5545.recurrenceset.RecurrenceRuleAdapter
import org.dmfs.rfc5545.recurrenceset.RecurrenceSet
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId

class RRuleSetProcessorDmfsAdaptor(
        private val recurrenceRuleCreator: (String) -> RecurrenceRule = defaultRecurrenceRuleCreator
) : RRuleProcessor {

    override fun computeNextDate(startDate: LocalDate, rrule: String, exrrule: String): LocalDate {
        require(exrrule, String::isNotBlank)

        val startDateTime = startDate.toDateTime()
        val rruleSet = RecurrenceSet().apply {
            addInstances(RecurrenceRuleAdapter(recurrenceRuleCreator(rrule)))
            addExceptions(RecurrenceRuleAdapter(recurrenceRuleCreator(exrrule)))
            // For unknown reason when iterating over recurrence set, start date is always
            // produced as first result
            addExceptions(RecurrenceList(longArrayOf(startDateTime.timestamp)))
        }

        val iterator = rruleSet.iterator(startDateTime.timeZone, startDateTime.timestamp)
        if (!iterator.hasNext()) {
            error("RRule $rrule with exception rule $exrrule does not produce any result")
        }
        val nextTs = rruleSet.iterator(startDateTime.timeZone, startDateTime.timestamp).next()
        return DateTime(nextTs).toLocalDate(ZoneId.systemDefault())
    }


    companion object {
        internal val defaultRecurrenceRuleCreator
                : (String) -> RecurrenceRule = { rrule -> RecurrenceRule(rrule, RecurrenceRule.RfcMode.RFC5545_STRICT) }
    }
}