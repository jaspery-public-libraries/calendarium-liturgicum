# TEMPORAL CYCLE - PROPER OF THE SEASON

## Definition

> In the Roman Missal the liturgical feasts of the year, as follows:
> Advent, Christmas season, Lent, Holy Week, Easter Sunday through Tuesday,
> Easter season, the season of the year, and solemnities of the Lord throughout
> the year.

## Source

Universal Norms on the Liturgical Year and the New General Roman Calendar
http://www.catholicculture.org/culture/library/view.cfm?id=10842

## Liturgical Seasons

### ADVENT

40\. Advent begins with First Vespers (Evening Prayer I) of the Sunday that
falls on or closest to 30 November and it ends before First Vespers (Evening
Prayer I) of Christmas.

41\. The Sundays of this time of year are named the First, Second, Third, and
Fourth Sundays of Advent.

### Christmas Time

33\. Christmas Time runs from First Vespers (Evening Prayer I) of the Nativity
of the Lord up to and including the Sunday after Epiphany or after 6 January.

### Ordinary Time #1 and #2

44\. Ordinary Time begins on the Monday which follows the Sunday occurring after
6 January and extends up to and including the Tuesday before the beginning of
Lent; it begins again on the Monday after Pentecost Sunday and ends before First
Vespers (Evening Prayer I) of the First Sunday of Advent

### LENT
28\. The forty days of Lent run from Ash Wednesday up to but excluding the Mass
of the Lord''s Supper exclusive.

### THE THE PASCHAL TRIDUUM AND THE EASTER TIME

19\. The Paschal Triduum of the Passion and Resurrection of the Lord begins with
the evening Mass of the Lord''s Supper, has its center in the Easter Vigil, and
closes with Vespers (Evening Prayer) of the Sunday of the Resurrection.

## Notes about Epiphany and Feast of Baptism of the Lord

However, in countries (such as the United States) where the celebration of
Epiphany is transferred to Sunday, sometimes the two feasts will fall on the
same day. In those years, the Baptism of the Lord is transferred to the
following day (Monday).

Baptism of the Lord – Feast: Sunday after Epiphany (or, if Epiphany is
celebrated on 7 or 8 January, the following Monday).

That this is not the case is also shown from the fact that the feast is
sometimes celebrated on a Monday that is Jan. 9. This happens only in those
countries that transfer the Epiphany to the Sunday between Jan. 2 and 8. When
Christmas Day falls on a Sunday, Epiphany falls on Jan. 8, and so the Christmas
season ends the following day.

In 2017 we have a unique situation. If Epiphany is transferred to the Sunday
that falls on January 7th or 8th, then the Feast of the Baptism of Jesus is
celebrated on the following Monday instead of the next Sunday. So, this year,
Ordinary Time begins on the Tuesday after Epiphany.

## Notes about Easter Time

II. Easter Time

22\. The fifty days from the Sunday of the Resurrection to Pentecost Sunday are
celebrated in joy and exultation as one feast day, indeed as one "great Sunday."

These are the days above all others in which the Alleluia is sung.

23\. The Sundays of this time of year are considered to be Sundays of Easter and
are called, after Easter Sunday itself, the Second, Third, Fourth, Fifth, Sixth,
and Seventh Sundays of Easter. This sacred period of fifty days concludes with
Pentecost Sunday.

24\. The first eight days of Easter Time constitute the Octave of Easter and are
celebrated as Solemnities of the Lord.

25\. On the fortieth day after Easter the Ascension of the Lord is celebrated,
except where, not being observed as a Holyday of Obligation, it has been
assigned to the Seventh Sunday of Easter.

26\. The weekdays from the Ascension up to and including the Saturday before
Pentecost prepare for the coming of the Holy Spirit, the Paraclete.
