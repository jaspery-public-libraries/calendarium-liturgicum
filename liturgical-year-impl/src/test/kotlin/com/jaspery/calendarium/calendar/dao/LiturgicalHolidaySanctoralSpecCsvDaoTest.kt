package com.jaspery.calendarium.calendar.dao

import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test
import java.net.URL

class LiturgicalHolidaySanctoralSpecCsvDaoTest {
    private val ALL_SAINTS_DATA = listOf(
            "1.11", "allSaints", "Solemnity", "1.12", "All Saints"
    )

    private val url: URL = mockk()
    private val dao = LiturgicalHolidaySanctoralSpecCsvDao(url)

    @Test
    fun testCreateBeanFromLine() {
        val bean = dao.createBeanFromLine(ALL_SAINTS_DATA)

        assertThat(bean.date).isEqualTo("1.11")
        assertThat(bean.holidayId).isEqualTo("allSaints")
        assertThat(bean.rank).isEqualTo("Solemnity")
        assertThat(bean.rankNum).isEqualTo("1.12")
        assertThat(bean.nameEn).isEqualTo("All Saints")
    }
}