/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.resource

import com.jaspery.calendarium.calendar.resource.ResourceBundleUtf8ControlProvider.Companion.FMT_JAVA_CLASS
import com.jaspery.calendarium.calendar.resource.ResourceBundleUtf8ControlProvider.Companion.FMT_JAVA_PROPS
import com.jaspery.calendarium.calendar.resource.ResourceBundleUtf8ControlProvider.UTF8Control
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkClass
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatIllegalArgumentException
import org.testng.annotations.Test
import java.io.ByteArrayInputStream
import java.net.URL
import java.net.URLConnection
import java.util.*

class ResourceBundleUtf8ControlProviderTest {
    private val resourceBundleControlProvider = ResourceBundleUtf8ControlProvider()

    @Test
    fun testResourceBundleControlProvider() {
        val baseName = "anyName"
        val control = resourceBundleControlProvider.getControl(baseName)

        assertThat(control).isSameAs(UTF8Control)
    }

    @Test
    fun testUTF8ControlNoReload() {
        val loader: ClassLoader = mockk()
        val inputStream = ByteArrayInputStream("".toByteArray(Charsets.UTF_8))

        every { loader.getResourceAsStream("baseName_en_US.properties") } returns inputStream

        val resourceBundle = UTF8Control.newBundle("baseName", Locale.US, FMT_JAVA_PROPS, loader, false)
        assertThat(resourceBundle!!.keySet()).isEmpty()

        verify(exactly = 1) { loader.getResourceAsStream("baseName_en_US.properties") }
    }

    @Test
    fun testUTF8ControlReload() {
        val loader: ClassLoader = mockk()
        val url: URL = mockk()
        val urlConnection: URLConnection = mockk()
        val inputStream = ByteArrayInputStream("".toByteArray(Charsets.UTF_8))

        every { loader.getResource("baseName_en_US.properties") } returns url
        every { url.openConnection() } returns urlConnection
        every { urlConnection.useCaches = false } returns Unit
        every { urlConnection.getInputStream() } returns inputStream

        val resourceBundle = UTF8Control.newBundle("baseName", Locale.US, FMT_JAVA_PROPS, loader, true)
        assertThat(resourceBundle!!.keySet()).isEmpty()


        verify(exactly = 1) { loader.getResource("baseName_en_US.properties") }
        verify(exactly = 1) { url.openConnection() }
        verify(exactly = 1) { urlConnection.useCaches = false }
        verify(exactly = 1) { urlConnection.getInputStream() }
    }

    @Test
    fun testUTF8ControlContract() {
        val baseName = "baseName"
        val format = FMT_JAVA_CLASS
        val locale = Locale.US
        val loader = mockkClass(ClassLoader::class)

        assertThatIllegalArgumentException().isThrownBy {
            UTF8Control.newBundle("", locale, format, loader, false)
        }

        assertThatIllegalArgumentException().isThrownBy {
            UTF8Control.newBundle(baseName, locale, "", loader, false)
        }.withMessageStartingWith("Unknown format: ")

        assertThatIllegalArgumentException().isThrownBy {
            UTF8Control.newBundle(baseName, locale, "unknown.format", loader, false)
        }.withMessageStartingWith("Unknown format: unknown.format")

        assertThat(
                UTF8Control.newBundle(baseName, locale, format, loader, false)
        ).isNull()
    }
}
