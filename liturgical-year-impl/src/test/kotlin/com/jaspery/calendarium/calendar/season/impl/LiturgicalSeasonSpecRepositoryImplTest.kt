/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.dao.LiturgicalSeasonSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalSeasonSpecCsvDao.LiturgicalSeasonSpecDto
import com.jaspery.calendarium.calendar.season.LiturgicalSeasonSpecRepository
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import com.jaspery.threetenbp.ext.monthDay
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.InstanceOfAssertFactories.STRING
import org.testng.annotations.BeforeMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.threeten.bp.MonthDay

class LiturgicalSeasonSpecRepositoryImplTest {
    private val liturgicalSeasonSpecDao: LiturgicalSeasonSpecCsvDao = mockk()
    private lateinit var liturgicalSeasonSpecRepo: LiturgicalSeasonSpecRepository

    @BeforeMethod
    fun clearMocks() = clearMocks(liturgicalSeasonSpecDao)

    @BeforeMethod
    fun initRepo() {
        liturgicalSeasonSpecRepo = LiturgicalSeasonSpecRepositoryImpl(liturgicalSeasonSpecDao)
    }

    @Test
    fun testGetLiturgicalSeasonsSpec() {
        every { liturgicalSeasonSpecDao.getAll() } returns SEASON_DATA_2

        val litSeasonsSpec = liturgicalSeasonSpecRepo.liturgicalSeasonsSpec
        assertThat(litSeasonsSpec).size().isEqualTo(2)

        verify(exactly = 1) { liturgicalSeasonSpecDao.getAll() }
    }

    @Test
    fun testGetLiturgicalSeasonSpecById() {
        every { liturgicalSeasonSpecDao.getAll() } returns SEASON_DATA_2

        val adventSpec = liturgicalSeasonSpecRepo.liturgicalSeasonSpecById["advent"]
        assertThat(adventSpec).isNotNull.extracting("seasonId").asInstanceOf(STRING).contains("advent")

        verify(exactly = 1) { liturgicalSeasonSpecDao.getAll() }
    }

    @Test(dataProvider = "findSeasonSpecCandidatesDataProvider")
    fun testFindSeasonSpecCandidatesAdvent(monthDay: MonthDay, seasonId: String) {
        every { liturgicalSeasonSpecDao.getAll() } returns SEASON_DATA_2

        val candidates = liturgicalSeasonSpecRepo.findSeasonSpecCandidates(monthDay)

        assertThat(candidates).hasSize(1)
        assertThat(candidates.single().seasonId).isEqualTo(seasonId)

        verify(exactly = 1) { liturgicalSeasonSpecDao.getAll() }
    }

    @DataProvider
    fun findSeasonSpecCandidatesDataProvider() = dataProvider {
        scenario("05.12".monthDay, "advent")
        scenario("26.12".monthDay, "christmasTime")
        scenario("02.01".monthDay, "christmasTime")
    }.testNGDataArray()

    companion object {
        private val SEASON_DATA_2 = listOf(
                LiturgicalSeasonSpecDto(
                        "27.11-3.12", "24.12", "advent",
                        "Violet", "Advent", true,
                        "FREQ = YEARLY;BYDAY=-5SU", "FREQ=YEARLY;BYMONTH=12;BYMONTHDAY=24",
                        stWVesp = true, enBfVesp = true
                ),
                LiturgicalSeasonSpecDto(
                        "25.12", "7.01-13.01", "christmasTime",
                        "White", "Christmas Time", true,
                        "FREQ=YEARLY;BYMONTH=12;BYMONTHDAY=25", "FREQ=YEARLY;BYMONTH=1;BYMONTHDAY=7,8,9,10,11,12,13;BYDAY=SU",
                        stWVesp = true, enBfVesp = false
                )
        )
    }
}