package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.dao.LiturgicalHolidaySanctoralSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalHolidaySanctoralSpecCsvDao.LiturgicalHolidaySanctoralSpecDto
import com.jaspery.calendarium.calendar.dao.LiturgicalHolidayTemporalSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalHolidayTemporalSpecCsvDao.LiturgicalHolidayTemporalSpecDto
import com.jaspery.calendarium.calendar.season.LiturgicalHolidaySpecRepository
import com.jaspery.threetenbp.ext.parseDayDotMonth
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

class LiturgicalHolidaySpecRepositoryImplTest {
    private val temporalHolidaysDao: LiturgicalHolidayTemporalSpecCsvDao = mockk()
    private val sanctoralHolidaysDao: LiturgicalHolidaySanctoralSpecCsvDao = mockk()
    private lateinit var repository: LiturgicalHolidaySpecRepository

    @BeforeMethod
    fun resetMocks() = clearMocks(temporalHolidaysDao, sanctoralHolidaysDao)

    @BeforeMethod()
    fun initRepository() {
        repository = LiturgicalHolidaySpecRepositoryImpl(temporalHolidaysDao, sanctoralHolidaysDao)
    }

    @Test
    fun testGetTemporalHolidaySpec() {
        every { temporalHolidaysDao.getAll() } returns SOME_TEMP_HOLIDAYS_SPECS_DEC

        val specs = repository.temporalHolidaySpec

        assertThat(specs).isNotEmpty

        verify { temporalHolidaysDao.getAll() }
    }

    @Test
    fun testGetSanctoralHolidaySpec() {
        every { sanctoralHolidaysDao.getAll() } returns SOME_SANCT_HOLIDAYS_SPEC_NOV

        val specs = repository.sanctoralHolidaySpec

        assertThat(specs).isNotEmpty

        verify { sanctoralHolidaysDao.getAll() }
    }

    @Test
    fun testFindHolidaySpecCandidatesNov() {
        every { temporalHolidaysDao.getAll() } returns SOME_TEMP_HOLIDAYS_SPECS_DEC
        every { sanctoralHolidaysDao.getAll() } returns SOME_SANCT_HOLIDAYS_SPEC_NOV

        val specs = repository.findHolidaySpecCandidates(parseDayDotMonth("28.11"))

        assertThat(specs).isNotEmpty.hasSize(1)
        assertThat(specs.single().holidayId).isEqualTo("adventSunday1st")

        verify { temporalHolidaysDao.getAll() }
        verify { sanctoralHolidaysDao.getAll() }
    }


    @Test
    fun testDefaultHolidaySpec() {
        val spec = repository.defaultHolidaySpec(parseDayDotMonth("28.11"))

        assertThat(spec.nameEn).isEqualTo("Ordinary Weekday")
    }

    private companion object {
        private val SOME_TEMP_HOLIDAYS_SPECS_DEC = listOf(
                LiturgicalHolidayTemporalSpecDto("27.11-03.12", "adventSunday1st", "advent", "Solemnity", "1.6", "First Sunday of Advent", "FREQ=YEARLY;BYDAY=-5SU", "", "Fist Sunday of Advent"),
                LiturgicalHolidayTemporalSpecDto("04.12-10.12", "adventSunday2nd", "advent", "Solemnity", "1.6", "Second Sunday of Advent", "FREQ=YEARLY;BYDAY=-4SU", "", "Second Sunday of Advent"),
                LiturgicalHolidayTemporalSpecDto("11.12-17.12", "adventSunday3rd", "advent", "Solemnity", "1.6", "Third Sunday of Advent", "FREQ=YEARLY;BYDAY =-3SU", "", "Third Sunday of Advent"),
                LiturgicalHolidayTemporalSpecDto("18.12-24.12", "adventSunday4th", "advent", "Solemnity", "1.6", "Fourth Sunday of Advent", "FREQ=YEARLY;BYDAY=-2SU", "", "Fourth Sunday of Advent")
        )

        private val SOME_SANCT_HOLIDAYS_SPEC_NOV = listOf(
                LiturgicalHolidaySanctoralSpecDto("1.11", "allSaints", "Solemnity", "3.7", "All Saints"),
                LiturgicalHolidaySanctoralSpecDto("2.11", "allSouls", "Memorial (O)", "3.7", "All Souls"),
                LiturgicalHolidaySanctoralSpecDto("3.11", "martinPorres", "Memorial (O)", "3.7", "Saint Martin de Porres, religious"),
                LiturgicalHolidaySanctoralSpecDto("4.11", "charlesBorromeo", "Memorial", "3.7", "Saint Charles Borromeo, bishop")
        )
    }
}
