package com.jaspery.calendarium.calendar.season.model

import com.jaspery.kotlin.testng.dataprovider.dataProvider
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatIllegalArgumentException
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

@ExperimentalUnsignedTypes
class LiturgicalDayPrecedenceComparisonTest {
    private val String.pn: LiturgicalDayPrecedence get() = LiturgicalDayPrecedence.valueOf(this)

    @Test
    fun `test paragraph number comparison`() {
        assertThat("1.12".pn).isGreaterThan("1.1".pn)
        assertThat("1.12".pn).isGreaterThan("1.2".pn)
        assertThat("2.12".pn).isGreaterThan("1.2".pn)
    }

    @DataProvider
    fun liturgicalDayPrecedenceParseDataProvider() = dataProvider {
        scenario("1.1", LiturgicalDayPrecedence(1u, 1u))
        scenario("1.12", LiturgicalDayPrecedence(1u, 12u))
        scenario("2.35", LiturgicalDayPrecedence(2u, 35u))
    }.testNGDataArray()

    @DataProvider
    fun liturgicalDayPrecedenceParseDataProviderPreconditions() = dataProvider {
        scenario("-1.1")
        scenario("1.512")
        scenario("2.35.123")
    }.testNGDataArray()

    @Test(dataProvider = "liturgicalDayPrecedenceParseDataProvider")
    fun `test paragraph number parse`(string: String, liturgicalDayPrecedence: LiturgicalDayPrecedence) {
        assertThat(LiturgicalDayPrecedence.valueOf(string)).isEqualTo(liturgicalDayPrecedence)
    }

    @Test(dataProvider = "liturgicalDayPrecedenceParseDataProvider")
    fun `test paragraph number toString`(string: String, liturgicalDayPrecedence: LiturgicalDayPrecedence) {
        assertThat(liturgicalDayPrecedence.toString()).isEqualTo(string)
    }

    @Test(dataProvider = "liturgicalDayPrecedenceParseDataProviderPreconditions")
    fun `test paragraph number parse fail precondition`(string: String) {
        assertThatIllegalArgumentException().isThrownBy {
            LiturgicalDayPrecedence.parse(string)
        }
    }
}