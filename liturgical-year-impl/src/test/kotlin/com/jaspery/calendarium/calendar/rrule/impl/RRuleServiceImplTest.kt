package com.jaspery.calendarium.calendar.rrule.impl

import com.jaspery.calendarium.calendar.rrule.RRuleService
import com.jaspery.threetenbp.ext.date
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatIllegalArgumentException
import org.testng.annotations.Test

class RRuleServiceImplTest {
    private val rruleProcessorFactory: RRuleProcessorFactory = mockk()
    private val service: RRuleService = RRuleServiceImpl(rruleProcessorFactory)

    @Test
    fun testComputeNextDate() {
        every { rruleProcessorFactory.createRRuleProcessor(RRULE).computeNextDate(any(), RRULE) } returns "2018-12-03".date
        val date = service.computeNextDate("2018-12-01".date, RRULE)

        assertThat(date).isEqualTo("2018-12-03".date)

        verify { rruleProcessorFactory.createRRuleProcessor(RRULE).computeNextDate(any(), RRULE) }
    }

    @Test
    fun testComputeNextDateEmpryRrule() {
        assertThatIllegalArgumentException().isThrownBy {
            service.computeNextDate("2018-12-01".date, "")
        }
    }

    companion object {
        private const val RRULE = "FREQ=YEARLY;BYDAY=-5SU"
    }
}