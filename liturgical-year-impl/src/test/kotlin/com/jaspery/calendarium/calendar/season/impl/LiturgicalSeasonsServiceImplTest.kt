/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.rrule.RRuleService
import com.jaspery.calendarium.calendar.season.LiturgicalSeasonSpecRepository
import com.jaspery.calendarium.calendar.season.LiturgicalSeasonsService
import com.jaspery.calendarium.calendar.season.model.LiturgicalSeason
import com.jaspery.calendarium.calendar.season.model.LiturgicalSeasonSpec
import com.jaspery.calendarium.calendar.season.model.LiturgicalYear
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import com.jaspery.threetenbp.ext.*
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.BeforeMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.Year

class LiturgicalSeasonsServiceImplTest {
    private val litSeasonSpecRepo: LiturgicalSeasonSpecRepository = mockk()
    private val litYearService: LiturgicalYearService = mockk()
    private val rruleService: RRuleService = mockk()
    private val litSeasonRepo: LiturgicalSeasonRepository = mockk()

    private val litSeasonsService: LiturgicalSeasonsService = LiturgicalSeasonsServiceImpl(litSeasonSpecRepo, litSeasonRepo, litYearService)

    @BeforeMethod
    fun resetMocks() = clearMocks(litSeasonSpecRepo, rruleService, litYearService, litSeasonRepo)

    @Test
    fun testGetSeasonsByYearAndFilterById() {
        val litSeasonsService = spyk(this.litSeasonsService)
        every { litSeasonsService.getSeasons(2018.year) } returns LIT_SEASONS_2018

        val seasons = litSeasonsService.getSeasons(2018.year, "ordinaryTime")

        assertThat(seasons).hasSize(2).extracting("seasonId").containsOnly("ordinaryTime")

        verify { litSeasonsService.getSeasons(2018.year) }
    }

    @Test(dataProvider = "liturgicalSeasonsData")
    fun testGetSeasons(spec: LiturgicalSeasonSpec, year: Year, stDt: LocalDate, enDt: LocalDate, season: LiturgicalSeason) {
        every { litYearService.getLiturgicalYear(year) } returns litYear2018
        every { litSeasonRepo.getSeasons(litYear2018) } returns listOf(season)

        val seasons = litSeasonsService.getSeasons(year)

        assertThat(seasons).size().isEqualTo(1)
        assertThat(seasons.first()).isEqualTo(season)

        verify(exactly = 1) { litYearService.getLiturgicalYear(year) }
        verify(exactly = 1) { litSeasonRepo.getSeasons(litYear2018) }
    }

    @Test
    fun testGetCurrentSeason() {
        val spec = LIT_SEASON_SPEC_LENT
        val seas = LIT_SEASON_2018_LENT

        every { litSeasonSpecRepo.findSeasonSpecCandidates("15.02".monthDay) } returns listOf(spec)
        every { litYearService.determineLiturgicalYear("2018-02-15".date) } returns litYear2018
        every { litSeasonRepo.getLiturgicalSeason(litYear2018, spec) } returns seas

        val season = litSeasonsService.getCurrentSeason("2018-02-15".date)

        assertThat(season).isEqualTo(seas)

        verify(exactly = 1) { litSeasonSpecRepo.findSeasonSpecCandidates(any()) }
        verify(exactly = 1) { litYearService.determineLiturgicalYear("2018-02-15".date) }
        verify(exactly = 1) { litSeasonRepo.getLiturgicalSeason(litYear2018, spec) }
    }

    @Test
    fun testGetCurrentSeasonAdvent() {
        val spec2 = LIT_SEASON_SPEC_ADVENT
        val seas2 = LIT_SEASON_2019_ADVENT

        every { litSeasonSpecRepo.findSeasonSpecCandidates("04.12".monthDay) } returns listOf(spec2)
        every { litYearService.determineLiturgicalYear("2018-12-04".date) } returns litYear2019
        every { litSeasonRepo.getLiturgicalSeason(litYear2019, spec2) } returns seas2

        val season = litSeasonsService.getCurrentSeason("2018-12-04".date)

        assertThat(season).isEqualTo(seas2)

        verify(exactly = 1) { litSeasonSpecRepo.findSeasonSpecCandidates("04.12".monthDay) }
        verify(exactly = 1) { litYearService.determineLiturgicalYear("2018-12-04".date) }
        verify(exactly = 1) { litSeasonRepo.getLiturgicalSeason(litYear2019, spec2) }
    }

    @Test(dataProvider = "weeksInSeasonDataProvider")
    fun testWeeksInSeason(season: LiturgicalSeason, numOfWeek: Int) {
        val service = spyk(litSeasonsService)
        every { service.getSeasons(2018.year) } returns LIT_SEASONS_2018

        val weeks = service.weeksInSeason(season)
        assertThat(weeks.toList()).hasSize(numOfWeek)

        verify(exactly = 1) { service.getSeasons(2018.year) }
    }

    @DataProvider
    fun liturgicalSeasonsData() = dataProvider {
        scenario(LIT_SEASON_SPEC_ADVENT, 2018.year, "2017-12-03".date, "2017-12-24".date, LIT_SEASON_2018_ADVENT)
        scenario(LIT_SEASON_SPEC_LENT, 2018.year, "2018-02-14".date, "2018-03-29".date, LIT_SEASON_2018_LENT)
    }.testNGDataArray()

    @DataProvider
    fun weeksInSeasonDataProvider() = dataProvider {
        scenario(LIT_SEASONS_2018[0], 4)
        scenario(LIT_SEASONS_2018[1], 3)
        scenario(LIT_SEASONS_2018[2], 34)
        scenario(LIT_SEASONS_2018[3], 7)
        scenario(LIT_SEASONS_2018[4], 1)
        scenario(LIT_SEASONS_2018[5], 7)
        scenario(LIT_SEASONS_2018[6], 34)
    }.testNGDataArray()

    companion object {
        private val LIT_SEASON_SPEC_ADVENT = LiturgicalSeasonSpec(
                MonthDayRange.parse("27.11-3.12"),
                MonthDayRange.parse("24.12"),
                "advent", "Violet", "Advent", true,
                "FREQ=YEARLY;BYDAY=-5SU", "FREQ=YEARLY;BYMONTH=12;BYMONTHDAY=24",
                true, true
        )

        private val LIT_SEASON_SPEC_LENT = LiturgicalSeasonSpec(
                MonthDayRange.parse("4.02-10.03"),
                MonthDayRange.parse("16.03-19.04"),
                "lent", "Violet", "Lent", false,
                "FREQ=YEARLY;BYEASTER=-46", "FREQ=YEARLY;BYEASTER=-3",
                false, true
        )

        private val LIT_SEASON_2018_ADVENT = LiturgicalSeason(
                LocalDateRange("2017-12-03".date, "2017-12-24".date),
                "advent", "Advent", 2018.year, true, true
        )

        private val LIT_SEASON_2018_LENT = LiturgicalSeason(
                LocalDateRange("2018-02-14".date, "2018-03-29".date),
                "lent", "Lent", 2018.year, false, true
        )

        private val LIT_SEASON_2019_ADVENT = LiturgicalSeason(
                LocalDateRange("2018-12-02".date, "2018-12-24".date),
                "advent", "Advent", 2019.year, true, true
        )

        private val LIT_SEASONS_2018 = listOf(
                LiturgicalSeason(LocalDateRange("2017-12-03".date, "2017-12-24".date), "advent", "Advent", 2018.year, true, true),
                LiturgicalSeason(LocalDateRange("2017-12-25".date, "2018-01-07".date), "christmasTime", "Christmas Time", 2018.year, true, false),
                LiturgicalSeason(LocalDateRange("2018-01-08".date, "2018-02-13".date), "ordinaryTime", "Ordinary Time", 2018.year, false, false),
                LiturgicalSeason(LocalDateRange("2018-02-14".date, "2018-03-29".date), "lent", "Lent", 2018.year, false, true),
                LiturgicalSeason(LocalDateRange("2018-03-29".date, "2018-03-31".date), "paschalTriduum", "The Paschal Triduum", 2018.year, true, true),
                LiturgicalSeason(LocalDateRange("2018-04-01".date, "2018-05-19".date), "easterTime", "The Easter Time", 2018.year, true, true),
                LiturgicalSeason(LocalDateRange("2018-05-21".date, "2018-12-01".date), "ordinaryTime", "Ordinary Time", 2018.year, false, true)
        )

        private val litYear2018 = LiturgicalYear(2018.year, LocalDateRange("2017-12-03".date, "2018-12-01".date))
        private val litYear2019 = LiturgicalYear(2019.year, LocalDateRange("2018-12-02".date, "2019-12-01".date))
    }
}
