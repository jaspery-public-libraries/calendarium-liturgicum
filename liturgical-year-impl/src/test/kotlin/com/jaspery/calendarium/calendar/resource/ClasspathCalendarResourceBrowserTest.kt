/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.resource

import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.CalendarFileName.TEMPORAL_SEASONS
import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.Rite.ROMAN
import io.mockk.*
import org.assertj.core.api.Assertions.*
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test
import java.util.*

class ClasspathCalendarResourceBrowserTest {
    private val classLoader: ClassLoader = mockk()
    private val calendarResourceBrowser: CalendarResourceBrowser = ClasspathCalendarResourceBrowser(classLoader, ResourceBundleUtf8ControlProvider.UTF8Control)

    private val calendarResourceBrowserNoControl: CalendarResourceBrowser = ClasspathCalendarResourceBrowser(classLoader)

    @BeforeMethod
    fun resetMocks() = clearMocks(classLoader)

    @Test
    fun testGeneralCalendarResource() {
        every { classLoader.getResource(any()) } returns null
        every { classLoader.getResource(EXPECTED_URL).toExternalForm() } returns EXPECTED_URL

        val temporalSeasonsUrl = calendarResourceBrowser.generalCalendarResource(ROMAN, TEMPORAL_SEASONS)

        assertThat(temporalSeasonsUrl.toExternalForm()).endsWith(EXPECTED_URL)

        verify(exactly = 5) { classLoader.getResource(any()) }
    }

    @Test
    fun testGeneralCalendarResourceMissing() {
        every { classLoader.getResource(any()) } returns null

        assertThatIllegalStateException().isThrownBy {
            calendarResourceBrowser.generalCalendarResource(ROMAN, TEMPORAL_SEASONS)
        }.withMessage("Classpath resource $TEMPORAL_SEASONS not found for rite $ROMAN")

        verify(exactly = 5) { classLoader.getResource(any()) }
    }

    @Test
    fun testGeneralCalendarResourceBundle() {
        mockkStatic(ResourceBundle::class)

        every { ResourceBundle.getBundle(any(), any(), any<ResourceBundle.Control>()) } returns mockkClass(ResourceBundle::class)

        val locale = Locale("uk")

        calendarResourceBrowser.generalCalendarResourceBundle(ROMAN, TEMPORAL_SEASONS, locale)

        val baseNameSlot = slot<String>()
        val localeSlot = slot<Locale>()
        val controlSlot = slot<ResourceBundle.Control>()

        verify { ResourceBundle.getBundle(capture(baseNameSlot), capture(localeSlot), capture(controlSlot)) }

        assertThat(baseNameSlot.captured).contains(ROMAN.riteId).contains(TEMPORAL_SEASONS.fileName)
        assertThat(localeSlot.captured).isSameAs(locale)
        assertThat(controlSlot.captured).isSameAs(ResourceBundleUtf8ControlProvider.UTF8Control)

        unmockkStatic(ResourceBundle::class)
    }

    @Test
    fun testGeneralCalendarResourceBundleNoControl() {
        mockkStatic(ResourceBundle::class)

        every { ResourceBundle.getBundle(any(), any(), any<ResourceBundle.Control>()) } returns mockkClass(ResourceBundle::class)

        val locale = Locale("uk")

        calendarResourceBrowserNoControl.generalCalendarResourceBundle(ROMAN, TEMPORAL_SEASONS, locale)

        val baseNameSlot = slot<String>()
        val localeSlot = slot<Locale>()

        verify { ResourceBundle.getBundle(capture(baseNameSlot), capture(localeSlot)) }

        assertThat(baseNameSlot.captured).contains(ROMAN.riteId).contains(TEMPORAL_SEASONS.fileName)
        assertThat(localeSlot.captured).isSameAs(locale)

        unmockkStatic(ResourceBundle::class)
    }

    @Test
    fun testNotImplemented() {
        assertThatThrownBy { calendarResourceBrowser.calendarVariants(ROMAN) }.isInstanceOf(NotImplementedError::class.java)
        assertThatThrownBy { calendarResourceBrowser.calendarFamilies(ROMAN) }.isInstanceOf(NotImplementedError::class.java)

        assertThatThrownBy { calendarResourceBrowser.calendarVariantResources(ROMAN) }.isInstanceOf(NotImplementedError::class.java)
        assertThatThrownBy { calendarResourceBrowser.calendarFamilyResources(ROMAN) }.isInstanceOf(NotImplementedError::class.java)

        assertThatThrownBy { calendarResourceBrowser.calendarVariantResourceBundle(ROMAN, "", TEMPORAL_SEASONS) }.isInstanceOf(NotImplementedError::class.java)
        assertThatThrownBy { calendarResourceBrowser.calendarFamilyResourceBundle(ROMAN, "", TEMPORAL_SEASONS) }.isInstanceOf(NotImplementedError::class.java)
        assertThatThrownBy { calendarResourceBrowser.calendarVariantNamesResourceBundle(ROMAN) }.isInstanceOf(NotImplementedError::class.java)
        assertThatThrownBy { calendarResourceBrowser.calendarFamilyNamesResourceBundle(ROMAN) }.isInstanceOf(NotImplementedError::class.java)
    }

    companion object {
        private const val EXPECTED_URL = "calendarium/roman/general/temporal-seasons.csv"
    }
}