package com.jaspery.calendarium.calendar.dao

import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test
import java.net.URL

class LiturgicalSeasonCsvDaoTest {
    private val ADVENT_DATA = listOf(
            "27.11-3.12", "24.12", "advent", "Violet", "Advent", "true",
            "FREQ=YEARLY;BYDAY=-5SU",
            "FREQ=YEARLY;BYMONTH=12;BYMONTHDAY=24", "true", "true"
    )

    private val url: URL = mockk()
    private val dao = LiturgicalSeasonSpecCsvDao(url)

    @Test
    fun testCreateBeanFromLine() {
        val bean = dao.createBeanFromLine(ADVENT_DATA)

        assertThat(bean.stDayRange).isEqualTo("27.11-3.12")
        assertThat(bean.endDayRange).isEqualTo("24.12")
        assertThat(bean.seasonId).isEqualTo("advent")
        assertThat(bean.primColor).isEqualTo("Violet")
        assertThat(bean.nameEn).isEqualTo("Advent")
        assertThat(bean.inPrevYear).isTrue()
        assertThat(bean.startDayRRULE).isEqualTo("FREQ=YEARLY;BYDAY=-5SU")
        assertThat(bean.endDayRRULE).isEqualTo("FREQ=YEARLY;BYMONTH=12;BYMONTHDAY=24")
        assertThat(bean.stWVesp).isTrue()
        assertThat(bean.enBfVesp).isTrue()
    }
}