package com.jaspery.calendarium.calendar.season.model

import com.jaspery.kotlin.testng.dataprovider.dataProvider
import com.jaspery.threetenbp.ext.LocalDateRange
import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.year
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

class LiturgicalSeasonTest {
    @Test(dataProvider = "seasonsDataProvider")
    fun testWeeks(season: LiturgicalSeason, numOfWeeks: Int) {
        assertThat(season.weeks().toList()).hasSize(numOfWeeks)
    }

    @DataProvider
    fun seasonsDataProvider() = dataProvider {
        scenario(LIT_SEASONS_2018[0], 4)
        scenario(LIT_SEASONS_2018[1], 3)
        scenario(LIT_SEASONS_2018[2], 6)
        scenario(LIT_SEASONS_2018[3], 7)
        scenario(LIT_SEASONS_2018[4], 1)
        scenario(LIT_SEASONS_2018[5], 7)
        scenario(LIT_SEASONS_2018[6], 28)
    }.testNGDataArray()

    private val LIT_SEASONS_2018 = listOf(
            LiturgicalSeason(LocalDateRange("2017-12-03".date, "2017-12-24".date), "advent", "Advent", 2018.year, true, true),
            LiturgicalSeason(LocalDateRange("2017-12-25".date, "2018-01-07".date), "christmasTime", "Christmas Time", 2018.year, true, false),
            LiturgicalSeason(LocalDateRange("2018-01-08".date, "2018-02-13".date), "ordinaryTime", "Ordinary Time", 2018.year, false, false),
            LiturgicalSeason(LocalDateRange("2018-02-14".date, "2018-03-29".date), "lent", "Lent", 2018.year, false, true),
            LiturgicalSeason(LocalDateRange("2018-03-29".date, "2018-03-31".date), "paschalTriduum", "The Paschal Triduum", 2018.year, true, true),
            LiturgicalSeason(LocalDateRange("2018-04-01".date, "2018-05-19".date), "easterTime", "The Easter Time", 2018.year, true, true),
            LiturgicalSeason(LocalDateRange("2018-05-21".date, "2018-12-01".date), "ordinaryTime", "Ordinary Time", 2018.year, false, true)
    )
}