package com.jaspery.calendarium.calendar.dao

import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test
import java.net.URL

class LiturgicalYearCsvDaoTest {
    private val YEAR_DATA = listOf(
            "27.11-3.12", "26.11-2.12", "FREQ=YEARLY;BYDAY=-5SU",
            "FREQ=YEARLY;BYDAY=SA;BYMONTHDAY=26,27,28,29,30,1,2;BYWEEKNO=-6,-5",
            "true", "advent"
    )

    private val url: URL = mockk()
    private val dao = LiturgicalYearSpecCsvDao(url)

    @Test
    fun testCreateBeanFromLine() {
        val bean = dao.createBeanFromLine(YEAR_DATA)

        assertThat(bean.stDayRange).isEqualTo("27.11-3.12")
        assertThat(bean.endDayRange).isEqualTo("26.11-2.12")
        assertThat(bean.startDayRRULE).isEqualTo("FREQ=YEARLY;BYDAY=-5SU")
        assertThat(bean.endDayRRULE).isEqualTo("FREQ=YEARLY;BYDAY=SA;BYMONTHDAY=26,27,28,29,30,1,2;BYWEEKNO=-6,-5")
        assertThat(bean.inPrevYear).isTrue()
        assertThat(bean.stSeasonId).isEqualTo("advent")
    }
}