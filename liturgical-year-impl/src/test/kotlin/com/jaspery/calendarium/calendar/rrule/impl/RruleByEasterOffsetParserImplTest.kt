/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.dmfs.rfc5545.recur.InvalidRecurrenceRuleException
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

class RruleByEasterOffsetParserImplTest {
    private val parser = RruleByEasterOffsetParserImpl()

    @Test(dataProvider = "provideByEasterRrulesValid")
    fun testParseValidByEasterRrule(rrule: String, expectedResult: Long) {
        assertThat(parser.isByEasterRule(rrule)).isTrue
        assertThat(parser.parseEasterRruleOffset(rrule)).isEqualTo(expectedResult)
    }

    @Test(dataProvider = "provideByEasterRrules")
    fun testInvalidByEasterRrule(rrule: String, isByEasterRrule: Boolean) {
        assertThat(parser.isByEasterRule(rrule)).isEqualTo(isByEasterRrule)

        assertThatExceptionOfType(
                InvalidRecurrenceRuleException::class.java
        ).isThrownBy {
            parser.parseEasterRruleOffset(rrule)
        }.withMessageStartingWith("Cannot parse by-date based rule")
    }

    @DataProvider
    fun provideByEasterRrules(): Iterator<Array<Any>> {
        return listOf<Array<Any>>(
                arrayOf("FREQ=YEARLY;BYEASTER=data", true),
                arrayOf("FREQ=YEARLY;BYEASTER=", true),
                arrayOf("FREQ=;BYEASTER=", false),
                arrayOf("FRE=YEARLY;BYEASTER=data", false),
                arrayOf("FREQ=YEARLY;BYEASTE=data", false),
                arrayOf("I like to run", false)
        ).iterator()
    }

    @DataProvider
    fun provideByEasterRrulesValid(): Iterator<Array<Any>> {
        return listOf<Array<Any>>(
                arrayOf("FREQ=YEARLY;BYEASTER=0", 0L),
                arrayOf("FREQ=YEARLY;BYEASTER=5", 5L),
                arrayOf("FREQ=YEARLY;BYEASTER=-2", -2L)
        ).iterator()
    }
}