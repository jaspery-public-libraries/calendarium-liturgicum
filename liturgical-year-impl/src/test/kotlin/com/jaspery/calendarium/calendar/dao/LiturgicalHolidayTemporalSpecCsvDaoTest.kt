package com.jaspery.calendarium.calendar.dao

import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test
import java.net.URL

class LiturgicalHolidayTemporalSpecCsvDaoTest {
    private val JC_KING_DATA = listOf(
            "20.11-26.11", "jesusKingUnivers", "ordinaryTime", "Solemnity", "1.11",
            "Our Lord JC, King of the Universe",
            "FREQ=YEARLY;BYMONTH=11;BYMONTHDAY=20,21,22,23,24,25,26;BYDAY=SU", "",
            "Last Sunday in Ordinary Time (last Sunday before 27 November)"
    )

    private val url: URL = mockk()
    private val dao = LiturgicalHolidayTemporalSpecCsvDao(url)

    @Test
    fun testCreateBeanFromLine() {
        val bean = dao.createBeanFromLine(JC_KING_DATA)

        assertThat(bean.dayRange).isEqualTo("20.11-26.11")
        assertThat(bean.holidayId).isEqualTo("jesusKingUnivers")
        assertThat(bean.season).isEqualTo("ordinaryTime")
        assertThat(bean.rank).isEqualTo("Solemnity")
        assertThat(bean.rankNum).isEqualTo("1.11")
        assertThat(bean.nameEn).isEqualTo("Our Lord JC, King of the Universe")
        assertThat(bean.rrule).isEqualTo("FREQ=YEARLY;BYMONTH=11;BYMONTHDAY=20,21,22,23,24,25,26;BYDAY=SU")
        assertThat(bean.exrule).isEmpty()
        assertThat(bean.ruleEn).isEqualTo("Last Sunday in Ordinary Time (last Sunday before 27 November)")
    }
}