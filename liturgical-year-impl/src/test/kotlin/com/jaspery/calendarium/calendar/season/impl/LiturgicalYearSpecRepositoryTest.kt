package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.dao.LiturgicalYearSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalYearSpecCsvDao.LiturgicalYearSpecDto
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test

class LiturgicalYearSpecRepositoryTest {
    private val dao: LiturgicalYearSpecCsvDao = mockk()
    private val repository = LiturgicalYearSpecRepository(dao)

    @Test
    fun test() {
        every { dao.getAll() } returns listOf(litYearSpecDto)

        assertThat(repository.liturgicalYearSpec.startDayRrule).isEqualTo(LIT_YEAR_START_RRULE)

        verify(exactly = 1) { dao.getAll() }
    }

    private companion object {
        private const val LIT_YEAR_START_RRULE = "FREQ=YEARLY;BYDAY=-5SU"

        private val litYearSpecDto = LiturgicalYearSpecDto(
                stDayRange = "27.11-3.12",
                endDayRange = "26.11-2.12",
                startDayRRULE = LIT_YEAR_START_RRULE,
                endDayRRULE = "",
                inPrevYear = true,
                stSeasonId = "advent"
        )
    }
}