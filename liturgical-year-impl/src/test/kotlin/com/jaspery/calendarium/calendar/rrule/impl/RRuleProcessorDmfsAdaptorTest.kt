/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import com.jaspery.kotlin.testng.dataprovider.dataProvider
import com.jaspery.threetenbp.ext.date
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.dmfs.rfc5545.recur.RecurrenceRule
import org.dmfs.rfc5545.recur.RecurrenceRuleIterator
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.threeten.bp.LocalDate

@Suppress("RemoveRedundantBackticks")
internal class RRuleProcessorDmfsAdaptorTest {
    @DataProvider
    fun computeNextDateDataProvider() = dataProvider {
        scenario("2018-03-01".date, "2018-03-13".date)
    }.testNGDataArray()

    @Test(dataProvider = "computeNextDateDataProvider")
    fun `test computeNextDate`(startDate: LocalDate, expectedResult: LocalDate) {
        val recurrenceRule: RecurrenceRule = mockk()
        val recurrenceRuleIterator: RecurrenceRuleIterator = mockk()
        val recurrenceRuleCreator: (String) -> RecurrenceRule = mockk()
        val recurrenceRuleProcessor = RRuleProcessorDmfsAdaptor(recurrenceRuleCreator)

        every { recurrenceRuleCreator.invoke(any()) } returns recurrenceRule
        every { recurrenceRule.iterator(any()) } returns recurrenceRuleIterator
        every { recurrenceRuleIterator.nextDateTime() } returns expectedResult.toDateTime()

        assertThat(
                recurrenceRuleProcessor.computeNextDate(startDate, "")
        ).isEqualTo(expectedResult)

        verify { recurrenceRuleIterator.nextDateTime() }
    }

    @DataProvider
    fun computeFirstSunOfAdvent2017DataProvider() = dataProvider {
        scenario("2017-09-01".date, RRULE_FIRST_SUN_OF_ADVENT, "2017-12-03".date)
    }.testNGDataArray()

    @Test(dataProvider = "computeFirstSunOfAdvent2017DataProvider")
    fun `test happy path - first sun of advent`(startDate: LocalDate, rrule: String, expectedResult: LocalDate) {
        val recurrenceRuleProcessor = RRuleProcessorDmfsAdaptor()

        assertThat(
                recurrenceRuleProcessor.computeNextDate(startDate, rrule)
        ).isEqualTo(expectedResult)
    }

    @Test
    fun `validate default recurrenceRuleCreator`() {
        val recurrenceRuleCreator = RRuleProcessorDmfsAdaptor.defaultRecurrenceRuleCreator
        val recurrenceRule = recurrenceRuleCreator(RRULE_FIRST_SUN_OF_ADVENT)

        assertThat(recurrenceRule.mode).isEqualTo(RecurrenceRule.RfcMode.RFC5545_STRICT)
    }

    companion object {
        private const val RRULE_FIRST_SUN_OF_ADVENT = "FREQ=YEARLY;BYDAY=-5SU"
    }
}
