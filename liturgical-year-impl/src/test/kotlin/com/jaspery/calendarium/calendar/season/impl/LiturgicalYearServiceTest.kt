package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.season.model.LiturgicalYear
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import com.jaspery.threetenbp.ext.LocalDateRange
import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.year
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.BeforeMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.Year

class LiturgicalYearServiceTest {
    private val repository: LiturgicalYearRepository = mockk()

    private val service: LiturgicalYearService = LiturgicalYearService(repository)

    @BeforeMethod
    fun resetMocks() = clearMocks(repository)

    @Test(dataProvider = "liturgical years data provider")
    fun `test getLiturgicalYear`(year: Year, expected: LiturgicalYear) {
        every { repository.getLiturgicalYear(2017.year) } returns litYear2017
        every { repository.getLiturgicalYear(2018.year) } returns litYear2018

        val litYear = service.getLiturgicalYear(year)
        assertThat(litYear).isEqualToComparingFieldByField(expected)

        verify(atLeast = 1, atMost = 2) { repository.getLiturgicalYear(any()) }
    }

    @Test(dataProvider = "liturgical years by date data provider")
    fun `test determineLiturgicalYear`(date: LocalDate, expected: LiturgicalYear) {
        every { repository.getLiturgicalYear(2017.year) } returns litYear2017
        every { repository.getLiturgicalYear(2018.year) } returns litYear2018

        val litYear = service.determineLiturgicalYear(date)
        assertThat(litYear).isEqualToComparingFieldByField(expected)

        verify(atLeast = 1, atMost = 2) { repository.getLiturgicalYear(any()) }
    }

    @DataProvider
    fun `liturgical years data provider`() = dataProvider {
        scenario(2018.year, litYear2018)
    }.testNGDataArray()

    @DataProvider
    fun `liturgical years by date data provider`() = dataProvider {
        scenario("2017-12-23".date, litYear2018)
        scenario("2018-01-15".date, litYear2018)
    }.testNGDataArray()

    private companion object {
        private val litYear2018 = LiturgicalYear(2018.year, LocalDateRange("2017-12-03".date, "2018-12-01".date))
        private val litYear2017 = LiturgicalYear(2017.year, LocalDateRange("2016-12-03".date, "2017-12-02".date))
    }
}
