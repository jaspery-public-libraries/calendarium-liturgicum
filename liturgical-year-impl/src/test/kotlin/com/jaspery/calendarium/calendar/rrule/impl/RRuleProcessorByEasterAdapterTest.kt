/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import com.jaspery.calendarium.computus.EasterComputus
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.plus
import com.jaspery.threetenbp.ext.year
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.*
import org.dmfs.rfc5545.recur.InvalidRecurrenceRuleException
import org.testng.annotations.BeforeMethod
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.threeten.bp.LocalDate

class RRuleProcessorByEasterAdapterTest {
    private val easterComputus: EasterComputus = mockk()
    private val rruleByEasterOffsetParser: RruleByEasterOffsetParserImpl = mockk()

    private val rruleProcessor: RRuleProcessor = RRuleProcessorByEasterAdapter(easterComputus, rruleByEasterOffsetParser)

    @BeforeMethod
    fun clearMocks() = clearMocks(rruleByEasterOffsetParser, easterComputus)

    @Test(dataProvider = "byEasterRruleDataProvider")
    fun `test computeNextDate for BYEASTER RRULE`(startDate: LocalDate, easterDate: LocalDate, easterOffset: Long, expectedResult: LocalDate) {
        every { rruleByEasterOffsetParser.parseEasterRruleOffset(BYEASTER_RRULE) } returns easterOffset
        every { easterComputus.compute(startDate.year.year) } returns easterDate

        val computeNextDate = rruleProcessor.computeNextDate(startDate, BYEASTER_RRULE)
        assertThat(computeNextDate).isEqualTo(expectedResult)

        verify(exactly = 1) { rruleByEasterOffsetParser.parseEasterRruleOffset(BYEASTER_RRULE) }
        verify(exactly = 1) { easterComputus.compute(startDate.year.year) }
    }

    @Test(dataProvider = "byEasterRruleDataProviderStDtPrevYear")
    fun `test computeNextDate for BYEASTER RRULE with start date in previous year`(startDate: LocalDate, easterDate: LocalDate, easterOffset: Long, expectedResult: LocalDate) {
        every { rruleByEasterOffsetParser.parseEasterRruleOffset(BYEASTER_RRULE) } returns easterOffset
        // return incorrect date, this does not matter for test
        every { easterComputus.compute(startDate.year.year) } returns easterDate.minusYears(1)
        every { easterComputus.compute(startDate.year.year + 1) } returns easterDate

        val computeNextDate = rruleProcessor.computeNextDate(startDate, BYEASTER_RRULE)
        assertThat(computeNextDate).isEqualTo(expectedResult)

        verify(exactly = 1) { rruleByEasterOffsetParser.parseEasterRruleOffset(BYEASTER_RRULE) }
        verify(exactly = 1) { easterComputus.compute(startDate.year.year + 1) }
        verify(exactly = 1) { easterComputus.compute(startDate.year.year) }
    }

    @Test
    fun `test invalid BYEASTER RRULE`() {
        every { rruleByEasterOffsetParser.parseEasterRruleOffset(BYEASTER_RRULE) } throws InvalidRecurrenceRuleException("Cannot parse Easter based rule $BYEASTER_RRULE")

        assertThatThrownBy {
            rruleProcessor.computeNextDate("2018-01-01".date, BYEASTER_RRULE)
        }.isInstanceOf(InvalidRecurrenceRuleException::class.java)

        verify(exactly = 1) { rruleByEasterOffsetParser.parseEasterRruleOffset(BYEASTER_RRULE) }
    }

    @Test
    fun `test EXRULE not supported for BYEASTER RRULE`() {
        assertThatIllegalArgumentException().isThrownBy {
            rruleProcessor.computeNextDate("2018-01-01".date, BYEASTER_RRULE, "FREQ=YEARLY;BYDAY=SU")
        }.withMessageStartingWith("EXRULE is not supported for BYEASTER based recurrence rules:")
    }

    @DataProvider
    fun byEasterRruleDataProvider() = dataProvider {
        scenario("2018-01-01".date, "2018-04-01".date, -2, "2018-03-30".date)
    }.testNGDataArray()

    @DataProvider
    fun byEasterRruleDataProviderStDtPrevYear() = dataProvider {
        scenario("2017-09-01".date, "2018-04-01".date, -2, "2018-03-30".date)
    }.testNGDataArray()

    companion object {
        private const val BYEASTER_RRULE = "FREQ=YEARLY;BYEASTER=-2"
    }
}