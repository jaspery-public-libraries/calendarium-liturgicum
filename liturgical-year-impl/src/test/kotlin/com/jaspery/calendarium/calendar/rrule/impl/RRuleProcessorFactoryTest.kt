/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.rrule.impl

import com.jaspery.calendarium.computus.EasterComputus
import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.year
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test
import org.threeten.bp.temporal.TemporalAdjusters

class RRuleProcessorFactoryTest {
    private val easterComputus: EasterComputus = mockk()
    private val rruleByEasterOffsetParser: RruleByEasterOffsetParserImpl = mockk()

    private val rRuleProcessorFactory = RRuleProcessorFactory(
            easterComputus, rruleByEasterOffsetParser
    )

    @Test
    fun testRRuleByEasterProcessorFactory() {
        val rrule = "SOME RRULE"
        val easterDate = "2018-04-01".date

        every { rruleByEasterOffsetParser.isByEasterRule(rrule) } returns true
        every { rruleByEasterOffsetParser.parseEasterRruleOffset(rrule) } returns 0
        every { easterComputus.compute(easterDate.year.year) } returns easterDate

        val processor = rRuleProcessorFactory.createRRuleProcessor(rrule)

        val result = processor.computeNextDate(easterDate.with(TemporalAdjusters.firstDayOfYear()), rrule)

        assertThat(result).isEqualTo(easterDate)

        verify(exactly = 1) { rruleByEasterOffsetParser.isByEasterRule(rrule) }
        verify(exactly = 1) { rruleByEasterOffsetParser.parseEasterRruleOffset(rrule) }
        verify(exactly = 1) { easterComputus.compute(easterDate.year.year) }
    }

    @Test
    fun testRRuleDmfsProcessorFactory() {
        val rrule = "FREQ=YEARLY"
        val eventDate = "2018-01-01".date

        every { rruleByEasterOffsetParser.isByEasterRule(rrule) } returns false

        val processor = rRuleProcessorFactory.createRRuleProcessor(rrule)

        val result = processor.computeNextDate(eventDate, rrule)

        assertThat(result).isEqualTo(eventDate)

        verify { rruleByEasterOffsetParser.isByEasterRule(rrule) }
    }

    @Test
    fun testFactoryWithDefaultDependencies() {
        val rrule = "FREQ=YEARLY"
        val eventDate = "2018-01-01".date

        val processor = RRuleProcessorFactory(easterComputus).createRRuleProcessor(rrule)

        val result = processor.computeNextDate(eventDate, rrule)

        assertThat(result).isEqualTo(eventDate)
    }
}