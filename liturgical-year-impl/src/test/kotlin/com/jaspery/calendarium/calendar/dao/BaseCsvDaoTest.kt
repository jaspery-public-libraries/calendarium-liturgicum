package com.jaspery.calendarium.calendar.dao

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test
import java.io.ByteArrayInputStream
import java.net.URL

class BaseCsvDaoTest {
    private val dataSourceUrl: URL = mockk()
    private val baseCsvDao = object : BaseCsvDao<BaseCsvTestBean>(dataSourceUrl) {
        override fun createBeanFromLine(strings: List<String>) = BaseCsvTestBean(strings.first())
    }

    @Test
    fun testGetAll() {
        every { dataSourceUrl.openStream() } returns ByteArrayInputStream(DATA.toByteArray())

        val dto = baseCsvDao.getAll().first()

        assertThat(dto.id).isEqualTo("1")

        verify(exactly = 1) { dataSourceUrl.openStream() }
    }

    private data class BaseCsvTestBean(val id: String)

    private companion object {
        private val DATA = """
            |ID;Name
            |1; Test Name
        """.trimMargin()
    }
}