package com.jaspery.calendarium.calendar.season.impl

import com.jaspery.calendarium.calendar.rrule.RRuleService
import com.jaspery.calendarium.calendar.season.LiturgicalHolidaySpecRepository
import com.jaspery.calendarium.calendar.season.LiturgicalHolidaysService
import com.jaspery.calendarium.calendar.season.LiturgicalSeasonsService
import com.jaspery.calendarium.calendar.season.model.LiturgicalDayPrecedence
import com.jaspery.calendarium.calendar.season.model.LiturgicalHolidaySpec
import com.jaspery.calendarium.calendar.season.model.LiturgicalHolidaySpec.SanctoralLiturgicalHolidaySpec
import com.jaspery.calendarium.calendar.season.model.LiturgicalSeason
import com.jaspery.threetenbp.ext.*
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

class LiturgicalHolidaysServiceImplTest {
    private val liturgicalSeasonsService: LiturgicalSeasonsService = mockk()
    private val repository: LiturgicalHolidaySpecRepository = mockk()
    private val rruleService: RRuleService = mockk()
    private val service: LiturgicalHolidaysService = LiturgicalHolidaysServiceImpl(liturgicalSeasonsService, repository, rruleService)

    @BeforeMethod
    fun resetMocks() = clearMocks(liturgicalSeasonsService, repository, rruleService)

    @Test
    fun testGetLiturgicalHoliday1stSunOfAdvent() {
        every { repository.findHolidaySpecCandidates("02.12".monthDay) } returns listOf(ADVENT_SUN_1ST)
        every { liturgicalSeasonsService.getCurrentSeason("2018-12-02".date) } returns ADVENT_2019
        every { liturgicalSeasonsService.weeksInSeason(ADVENT_2019) } returns ADVENT_2019_WEEKS
        every { rruleService.computeNextDate(any(), any()) } returns "2018-12-02".date

        val holiday = service.getLiturgicalHoliday("2018-12-02".date)

        assertThat(holiday.seasonId).isEqualTo("advent")
        assertThat(holiday.precedence.toString()).isEqualTo("1.6")

        verify(exactly = 1) { repository.findHolidaySpecCandidates(any()) }
        verify(exactly = 1) { liturgicalSeasonsService.getCurrentSeason(any()) }
        verify(exactly = 1) { liturgicalSeasonsService.weeksInSeason(any()) }
        verify(exactly = 1) { rruleService.computeNextDate(any(), any()) }
    }

    @Test
    fun testGetLiturgicalHolidayDefault() {
        every { repository.findHolidaySpecCandidates("03.12".monthDay) } returns listOf(ADVENT_SUN_1ST)
        every { repository.defaultHolidaySpec("03.12".monthDay) } returns SanctoralLiturgicalHolidaySpec("03.12".monthDay, "", LiturgicalDayPrecedence.valueOf("3.7", "memorial"), "")
        every { liturgicalSeasonsService.getCurrentSeason("2018-12-03".date) } returns ADVENT_2019
        every { liturgicalSeasonsService.weeksInSeason(ADVENT_2019) } returns ADVENT_2019_WEEKS
        every { rruleService.computeNextDate(any(), any()) } returns "2018-12-02".date

        val holiday = service.getLiturgicalHoliday("2018-12-03".date)

        assertThat(holiday.seasonId).isEqualTo("advent")
        assertThat(holiday.precedence.toString()).isEqualTo("3.7")

        verify(exactly = 1) { repository.findHolidaySpecCandidates(any()) }
        verify(exactly = 1) { repository.defaultHolidaySpec(any()) }
        verify(exactly = 1) { liturgicalSeasonsService.getCurrentSeason(any()) }
        verify(exactly = 1) { liturgicalSeasonsService.weeksInSeason(any()) }
        verify(exactly = 1) { rruleService.computeNextDate(any(), any()) }
    }

    private companion object {
        private val ADVENT_2019 = LiturgicalSeason(LocalDateRange("2018-12-02".date, "2018-12-24".date), "advent", "Advent", 2018.year, true, true)
        private val ADVENT_SUN_1ST = LiturgicalHolidaySpec.TemporalLiturgicalHolidaySpec(
                MonthDayRange.parse("27.11-03.12"),
                "adventSunday1st",
                LiturgicalDayPrecedence.valueOf("1.6", "Solemnity"),
                "First Sunday of Advent",
                "advent",
                "FREQ=YEARLY;BYDAY=-5SU", "",
                "Fist Sunday of Advent")
        private val ADVENT_2019_WEEKS = listOf(
                LocalDateRange("2018-12-02".date, "2018-12-08".date),
                LocalDateRange("2018-12-09".date, "2018-12-15".date),
                LocalDateRange("2018-12-16".date, "2018-12-22".date),
                LocalDateRange("2018-12-23".date, "2018-12-29".date)
        ).asSequence()
    }
}
