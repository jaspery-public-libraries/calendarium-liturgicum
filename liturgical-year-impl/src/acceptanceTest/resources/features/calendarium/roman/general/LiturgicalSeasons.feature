Feature:
  As a developer
  I need API to retrieve dates and details of all liturgical seasons in given year
  So that I can display them as overlay on calendar

  Background:
    Given Liturgical Seasons interface is initialized

  Scenario Outline: Liturgical Season List with details for some years
    When year is <year>
    And season sequence number is <seq_n>
    Then liturgical season at position seq_n in list is described with
      | <season_id> | <season_name> | <season_st_dt> | <season_en_dt> | <year> | <st_w_vesper> | <en_before_vesper> |
    Examples:
      | year | seq_n | season_id      | season_name         | season_st_dt | season_en_dt | year | st_w_vesper | en_before_vesper |
      | 2018 | 1     | advent         | Advent              | 2017-12-03   | 2017-12-24   | 2018 | true        | true             |
      | 2018 | 2     | christmasTime  | Christmas Time      | 2017-12-25   | 2018-01-07   | 2018 | true        | false            |
      | 2018 | 3     | ordinaryTime   | Ordinary Time       | 2018-01-08   | 2018-02-13   | 2018 | false       | false            |
      | 2018 | 4     | lent           | Lent                | 2018-02-14   | 2018-03-29   | 2018 | false       | true             |
      | 2018 | 5     | paschalTriduum | The Paschal Triduum | 2018-03-29   | 2018-03-31   | 2018 | true        | true             |
      | 2018 | 6     | easterTime     | The Easter Time     | 2018-04-01   | 2018-05-19   | 2018 | true        | true             |
      | 2018 | 7     | ordinaryTime   | Ordinary Time       | 2018-05-21   | 2018-12-01   | 2018 | false       | true             |

  Scenario Outline: Current Liturgical Season with details for some date
    When date is <date>
    Then liturgical season is described with
      | <season_id> | <season_name> | <season_st_dt> | <season_en_dt> | <year> | <st_w_vesper> | <en_before_vesper> |
    Examples:
      | date       | season_id    | season_name   | season_st_dt | season_en_dt | year | st_w_vesper | en_before_vesper |
      | 2018-10-21 | ordinaryTime | Ordinary Time | 2018-05-21   | 2018-12-01   | 2018 | false       | true             |
      | 2018-12-04 | advent       | Advent        | 2018-12-02   | 2018-12-24   | 2019 | true        | true             |
