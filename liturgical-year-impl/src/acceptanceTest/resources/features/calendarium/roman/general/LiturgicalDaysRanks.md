## Liturgical Days Ranks

### Table of Precedence:

  | Rank      | Rank before 1969| Precedence                                    |
  | --------- | ----------------| --------------------------------------------- |
  | Solemnity | I Class         |  1. Paschal Triduum                           |
  |           |                 |  2. Christmas                                 |
  |           |                 |  3. Pentecost                                 |
  |           |                 |  4. Epiphany                                  |
  |           |                 |  5. Ascension                                 |
  |           |                 |  6. Sundays of Advent and Lent                |
  |           |                 |  7. Sundays in the Easter Season              |
  |           | * Feria I Cls   |  8. Ash Wednesday                             |
  |           | * Feria I Cls   |  9. Weekdays of Holy Week                     |
  |           |                 | 10. Weekdays in the Easter Octave             |
  |           |                 | 11. Solemnities of the Lord in the general    |
  |           |                 |   Calendar (including also Solemnities of the |
  |           |                 |   Blessed Virgin Mary, and saints listed in   |
  |           |                 |   the general calendar                        |
  |           |                 | 12. All Soul's Day                            |
  |           |                 | 13. Proper Solemnities (*)                    |
  | --------- | --------------- | --------------------------------------------- |
  | Feast     | II Class        | 1. Feasts of the Lord in the general calendar |
  |           |                 | 2. Sundays of the Christmas season and        |
  |           |                 |   Sundays in ordinary time                    |
  |           |                 | 3. Feasts of the Blessed Virgin Mary and of   |
  |           |                 |   the saints in the general calendar          |
  |           |                 | 4. Proper feasts (*)                          |
  |           | * Feria II Cls  | 5. Weekdays of Advent from December 17 to     |
  |           |                 |   December 24 inclusive                       |
  |           |                 | 6. Days within the octave of Christmas        |
  |           | * Feria III Cls | 7. Weekdays of Lent                           |
  | --------- | --------------- | --------------------------------------------- |
  | Memorial  | III Class       | 1. Obligatory Memorials, General              |
  |           |                 | 2. Obligatory Memorials, Proper (*)           |
  |           |                 | 3. Optional Memorials (*)                     |
  |           | * Feria III Cls | 4. Weekdays of Advent up to December 16       |
  |           |                 |   (inclusive)                                 |
  |           |                 | 5. Weekdays of Christmas Season from January 2|
  |           |                 |   until Saturday after Epiphany               |
  |           |                 | 6. Weekdays of Easter Season from Monday after|
  |           |                 |   the Octave until Saturday before Pentecost  |
  |           | * Feria IV Cls  | 7. Weekdays in Ordinary Time                  |
  | --------- | --------------- | --------------------------------------------- |

  (*) Proper Solemnities, namely:
  1. Solemnity of the principal patron of the place, city, or state
  2. Solemnity of the dedication and anniversary of the dedication of a
  particular church
  3. Solemnity of the titular saint of a particular church
  4. Solemnity of the titular saint, founder, or principal patron of an order or
  congregation

  (*) Proper Feasts, namely:
  1. Feast of the principal patron of the diocese
  2. Feast of the anniversary of the dedication of the cathedral
  3. Feast of the principal patron of the territory, province, country, or more
  extensive territory
  4. Feast of the titular saint, founder, or principal patron of an order or
  congregation and religious province, observing the directives in description
  of Proper Solemnities
  5. Other feasts proper to an individual church
  6. Other feasts listed in the calendar of the diocese, order, or congregation

  (*) Proper obligatory memorials, namely:
  a) Memorial of a secondary patron of the place, diocese, region or province,
  country, or more extensive territory; or of an order, congregation, or
  religious province
  b) Obligatory memorials proper to an individual church
  c) Obligatory memorials listed in the calendar of a diocese, order, or 
  congregation

  (*) Optional memorials, as described in the instructions indicated for the
  Mass and office, may be observed even on the days in Feasts 5, 6, 7. In the
  same manner obligatory memorials may be celebrated as optional memorials if
  they happen to fall on the Lenten weekdays.

### Solemnity vs. Sunday

  If a solemnity fell on an Advent, Lent or Easter Sunday it would have to be transferred,
  usually to the next day. There are eighteen solemnities for the year, with only eight
  celebrated on a fixed date and not a Sunday. This makes only eight possible “eligible”
  solemnities. The table below applies these principles of precedence for each of these
  eight solemnities:
  
  | Date    | Solemnity Name                | Liturgical Season |Transferred or Sunday? |
  |---------|-------------------------------|-------------------|-----------------------|
  | Jan 1   | Mary, Mother of God           | Christmas         | Sunday
  | Mar 19  | St. Joseph (*)                | Lent              | Transferred
  | Mar 25  | Annunciation (*)              | Lent              | Transferred
  | June 24 | Nativity of John the Baptist  | Ordinary Time     | Sunday
  | June 29 | Saint Peter and Paul          | Ordinary Time     | Sunday
  | Aug 15  | Assumption                    | Ordinary Time     | Sunday
  | Nov 1   | All Saints                    | Ordinary Time     | Sunday
  | Dec 8   | Immaculate Conception         | Advent            | Transferred
  
  (*) There is a possibility of another kind of transference for St. Joseph and the Annunciation
  if the solemnity falls during Holy Week or the Octave of Easter. St. Joseph could be moved
  earlier or later, and the Annunciation would be moved to the Monday after the Octave of Easter
  or Divine Mercy Sunday.
  
### Ferias

#### 1960 - 1969

In addition to his division of festal days and Sundays, Pope John XXIII introduced a division of
ferias into four classes:

* First-class ferias, outranking all feasts: Ash Wednesday and all the weekdays of Holy Week.
* Second-class ferias, outranking local second-class feasts: ferias of Advent from 17 December to
23 December, and Ember Days of Advent, Lent and September.
* Third-class ferias: ferias in Lent from Thursday after Ash Wednesday to Saturday before the Second
Sunday of the Passion (Palm Sunday) except Ember Days (these outranked third-class feasts), and
ferias in Advent up to 16 December except Ember Days (these were outranked by third-class feasts).
* Fourth-class ferias: all other ferias

#### After 1969

* Seasonal Weekday — a weekday in a "strong" liturgical season (Advent, Christmastide, Lent, or Eastertide),
on which no solemnity, feast, or memorial happens to be observed. The equivalent in the Extraordinary Form
would be I, II and III Class Ferial days, and the even older Tridentine forms would be classified as Major
Ferials.
* Feria or Ferial Weekday — a weekday in ordinary time on which no solemnity, feast or memorial happens to
be observed. The equivalent in the Extraordinary Form would be a IV Class Ferial, and in the older Tridentine
forms would be Minor Ferials.