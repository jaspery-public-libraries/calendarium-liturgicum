Feature:
  As a developer
  I need API to retrieve dates and details of all liturgical holidays in given year
  So that I can display them as overlay on calendar

  Background:
    Given Liturgical Holidays interface is initialized

  Scenario: 29th of Nov 2018 is Thursday of the Thirty-Fourth Week of Ordinary Time
    When date passed to liturgical holidays service is 2018-11-29
    Then liturgical day is not null

  Scenario Outline: 29th of Nov 2018 is Thursday of the Thirty-Fourth Week of Ordinary Time
    When date passed to liturgical holidays service is 2018-11-29
    Then liturgical holiday is described with
      | <date> | <holidayId> | <seasonId> | <weekNo> | <rankNum> | <rankId> |
    Examples:
      | date       | holidayId | seasonId     | weekNo | rankNum | rankId   |
      | 2018-11-29 | feria     | ordinaryTime | 34     | 3.7     | memorial |

  Scenario Outline: 2nd, 9th, 16th and 23rd of Dec 2018 are Sundays of Advent
    When date passed to liturgical holidays service is <date>
    Then liturgical holiday is described with
      | <date> | <holidayId> | <seasonId> | <weekNo> | <rankNum> | <rankId> |
    Examples:
      | date       | holidayId       | seasonId | weekNo | rankNum | rankId    |
      | 2018-12-02 | adventSunday1st | advent   | 1      | 1.6     | solemnity |
      | 2018-12-09 | adventSunday2nd | advent   | 2      | 1.6     | solemnity |
      | 2018-12-16 | adventSunday3rd | advent   | 3      | 1.6     | solemnity |
      | 2018-12-23 | adventSunday4th | advent   | 4      | 1.6     | solemnity |

  Scenario Outline: 25th of Dec 2018 is Christmas
    When date passed to liturgical holidays service is <date>
    Then liturgical holiday is described with
      | <date> | <holidayId> | <seasonId> | <weekNo> | <rankNum> | <rankId> |
    Examples:
      | date       | holidayId | seasonId      | weekNo | rankNum | rankId    |
      | 2018-12-25 | christmas | christmasTime | 1      | 1.2     | solemnity |

  Scenario Outline: The Feast of the Holy Family: Sun within the Octave of Christmas (or, if there is no such Sun, 30 Dec).
  - The Feast of the Holy Family of Jesus, Mary, and Joseph is celebrated on the Sunday within the Octave of Christmas,
  that is, the Sunday between Christmas Day and New Year's Day (both exclusive), or if both Christmas Day and
  the Solemnity of Mary, Mother of God are Sundays, on 30 December (always a Friday in such years).
  - Date range: 26.12-31.12
    When date passed to liturgical holidays service is <date>
    Then liturgical holiday id is <holidayId>
    Examples:
      | date       | holidayId  |
      | 2010-12-26 | holyFamily |
      | 2011-12-30 | holyFamily |
      | 2012-12-30 | holyFamily |
      | 2013-12-29 | holyFamily |
      | 2014-12-28 | holyFamily |
      | 2015-12-27 | holyFamily |
      | 2016-12-30 | holyFamily |
      | 2017-12-31 | holyFamily |
      | 2018-12-30 | holyFamily |
      | 2019-12-29 | holyFamily |
      | 2020-12-27 | holyFamily |

  Scenario Outline: Test precedence between Feast of Holy Family and other feasts in Nov.
    When date passed to liturgical holidays service is <date>
    Then liturgical holiday is described with
      | <date> | <holidayId> | <seasonId> | <weekNo> | <rankNum> | <rankId> |
    Examples: # Christmas Time in 2018
      | date       | holidayId         | seasonId      | weekNo | rankNum | rankId    |
      | 2018-12-08 | immaculateConcBVM | advent        | 1      | 1.11    | solemnity |
      | 2018-12-25 | christmas         | christmasTime | 1      | 1.2     | solemnity |
      | 2018-12-26 | saintStephen      | christmasTime | 1      | 2.3     | feast     |
      | 2018-12-27 | johnTheApostle    | christmasTime | 1      | 2.3     | feast     |
      | 2018-12-28 | holyInnocents     | christmasTime | 1      | 2.3     | feast     |
      | 2018-12-30 | holyFamily        | christmasTime | 2      | 2.1     | feast     |
    Examples: # Overlap between Christmas, Holy Family and other feasts and memorials
      | date       | holidayId       | seasonId      | weekNo | rankNum | rankId    |
      | 2010-12-25 | christmas       | christmasTime | 1      | 1.2     | solemnity |
      | 2010-12-26 | holyFamily      | christmasTime | 2      | 2.1     | feast     |
      | 2010-12-27 | johnTheApostle  | christmasTime | 2      | 2.3     | feast     |
      | 2013-12-28 | holyInnocents   | christmasTime | 1      | 2.3     | feast     |
      | 2013-12-29 | holyFamily      | christmasTime | 2      | 2.1     | feast     |
      | 2013-12-30 | feria           | christmasTime | 2      | 3.7     | memorial  |
      | 2014-12-27 | johnTheApostle  | christmasTime | 1      | 2.3     | feast     |
      | 2014-12-28 | holyFamily      | christmasTime | 2      | 2.1     | feast     |
      | 2014-12-29 | thomasBecket    | christmasTime | 2      | 3.3     | memorial  |
      | 2015-12-26 | saintStephen    | christmasTime | 1      | 2.3     | feast     |
      | 2015-12-27 | holyFamily      | christmasTime | 2      | 2.1     | feast     |
      | 2015-12-28 | holyInnocents   | christmasTime | 2      | 2.3     | feast     |
      | 2017-12-30 | feria           | christmasTime | 1      | 3.7     | memorial  |
      | 2017-12-31 | holyFamily      | christmasTime | 2      | 2.1     | feast     |
      | 2018-12-29 | thomasBecket    | christmasTime | 1      | 3.3     | memorial  |
      | 2018-12-30 | holyFamily      | christmasTime | 2      | 2.1     | feast     |
      | 2018-12-31 | saintSylvesterI | christmasTime | 2      | 3.3     | memorial  |
  @ignored
    Examples: # this one does not work correctly yet
      | date       | holidayId | seasonId | weekNo | rankNum | rankId |
      | 2018-01-01 |           |          |        |         |        |


  Scenario Outline: Test precedence between Solemnity of Jesus Christ, King of the Universe and other feasts in Nov.
  - The Solemnity of Our Lord Jesus Christ, King of the Universe is observed on the final Sunday of Ordinary Time.
  - Date range: 20.11-26.11
    When date passed to liturgical holidays service is <date>
    Then liturgical holiday is described with
      | <date> | <holidayId> | <seasonId> | <weekNo> | <rankNum> | <rankId> |
    Examples:
      | date       | holidayId         | seasonId     | weekNo | rankNum | rankId    |
      | 2010-11-20 | feria             | ordinaryTime | 32     | 3.7     | memorial  |
      | 2010-11-21 | jesusKingUnivers  | ordinaryTime | 33     | 1.11    | solemnity |
      | 2010-11-22 | cecilia           | ordinaryTime | 33     | 3.1     | memorial  |
      | 2014-11-22 | cecilia           | ordinaryTime | 32     | 3.1     | memorial  |
      | 2014-11-23 | jesusKingUnivers  | ordinaryTime | 33     | 1.11    | solemnity |
      | 2014-11-24 | andrewDungLac     | ordinaryTime | 33     | 3.1     | memorial  |
      | 2016-11-20 | jesusKingUnivers  | ordinaryTime | 33     | 1.11    | solemnity |
      | 2016-11-21 | presentationMary  | ordinaryTime | 33     | 3.1     | memorial  |
      | 2017-11-25 | catherineAlexandr | ordinaryTime | 33     | 3.3     | memorial  |
      | 2017-11-26 | jesusKingUnivers  | ordinaryTime | 34     | 1.11    | solemnity |
      | 2018-11-24 | andrewDungLac     | ordinaryTime | 33     | 3.1     | memorial  |
      | 2018-11-25 | jesusKingUnivers  | ordinaryTime | 34     | 1.11    | solemnity |
      | 2018-11-26 | feria             | ordinaryTime | 34     | 3.7     | memorial  |
      | 2019-11-23 | columbani         | ordinaryTime | 32     | 3.3     | memorial  |
      | 2019-11-24 | jesusKingUnivers  | ordinaryTime | 33     | 1.11    | solemnity |
      | 2019-11-25 | catherineAlexandr | ordinaryTime | 33     | 3.3     | memorial  |
      | 2020-11-21 | presentationMary  | ordinaryTime | 32     | 3.1     | memorial  |
      | 2020-11-22 | jesusKingUnivers  | ordinaryTime | 33     | 1.11    | solemnity |
      | 2020-11-23 | columbani         | ordinaryTime | 33     | 3.3     | memorial  |

  Scenario Outline: Test ferias during Christmas time.
    When date passed to liturgical holidays service is <date>
    Then liturgical holiday is described with
      | <date> | <holidayId> | <seasonId> | <weekNo> | <rankNum> | <rankId> |
    Examples:
      | date       | holidayId | seasonId      | weekNo | rankNum | rankId   |
      | 2013-12-30 | feria     | christmasTime | 2      | 3.7     | memorial |
      | 2017-12-30 | feria     | christmasTime | 1      | 3.7     | memorial |
      | 2018-01-04 | feria     | christmasTime | 2      | 3.7     | memorial |
      | 2018-01-05 | feria     | christmasTime | 2      | 3.7     | memorial |
