/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.roman.general

import com.jaspery.calendarium.calendar.season.LiturgicalSeasonsService
import com.jaspery.calendarium.calendar.season.model.LiturgicalSeason
import io.cucumber.guice.ScenarioScoped
import io.cucumber.java8.En
import org.assertj.core.api.Assertions.assertThat
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.threeten.bp.LocalDate
import org.threeten.bp.Year
import javax.inject.Inject

@Suppress("unused")
class LiturgicalSeasonsServiceStepDefs @Inject constructor(context: LiturgicalSeasonCukesContext) : En {
    init {
        with(context) {
            Given("Liturgical Seasons interface is initialized") { assertThat(service).isNotNull() }

            When("year is {year}") { year: Year ->
                result = service.getSeasons(year)

                assertThat(result).isNotEmpty
                assertThat(result).size().isEqualByComparingTo(7)
            }

            When("date is {local_date}") { localDate: LocalDate ->
                result = listOf(service.getCurrentSeason(localDate))

                assertThat(result).isNotEmpty
                assertThat(result).size().isEqualByComparingTo(1)
            }
        }
    }

    @ScenarioScoped
    class LiturgicalSeasonCukesContext private constructor(
            var result: List<LiturgicalSeason>,
            var liturgicalSeason: LiturgicalSeason?
    ) : KoinComponent {
        constructor() : this(listOf(), null)

        val service: LiturgicalSeasonsService by inject()
    }
}