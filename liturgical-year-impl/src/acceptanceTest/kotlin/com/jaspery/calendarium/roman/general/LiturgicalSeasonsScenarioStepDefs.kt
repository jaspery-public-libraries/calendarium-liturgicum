/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.roman.general

import com.jaspery.calendarium.calendar.season.model.LiturgicalSeason
import com.jaspery.calendarium.roman.general.LiturgicalSeasonsServiceStepDefs.LiturgicalSeasonCukesContext
import io.cucumber.java8.En
import org.assertj.core.api.Assertions.assertThat
import javax.inject.Inject

@Suppress("unused")
class LiturgicalSeasonsScenarioStepDefs @Inject constructor(context: LiturgicalSeasonCukesContext) : En {
    init {
        with(context) {
            When("season sequence number is {int}") { seqNo: Int ->
                liturgicalSeason = result[seqNo - 1]

                assertThat(liturgicalSeason).isNotNull()
            }

            Then("liturgical season at position seq_n in list is described with") { season: LiturgicalSeason ->
                assertThat(liturgicalSeason).isEqualTo(season)
            }

            Then("liturgical season is described with") { season: LiturgicalSeason ->
                assertThat(result.single()).isEqualTo(season)
            }
        }
    }
}