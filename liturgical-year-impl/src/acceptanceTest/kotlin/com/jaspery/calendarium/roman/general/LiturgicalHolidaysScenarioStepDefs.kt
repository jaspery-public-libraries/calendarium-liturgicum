/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.roman.general

import com.jaspery.calendarium.calendar.season.model.LiturgicalHoliday
import com.jaspery.calendarium.roman.general.LiturgicalHolidaysServiceStepDefs.LiturgicalHolidaysCukesContext
import io.cucumber.java8.En
import org.assertj.core.api.Assertions.assertThat
import javax.inject.Inject

@Suppress("unused")
class LiturgicalHolidaysScenarioStepDefs @Inject constructor(context: LiturgicalHolidaysCukesContext) : En {
    init {
        with(context) {
            Then("liturgical holiday is described with") { holiday: LiturgicalHoliday ->
                assertThat(result).isEqualTo(holiday)
            }

            Then("^liturgical holiday id is (\\w+)$") { holidayId: String ->
                assertThat(result?.holidayId).isEqualTo(holidayId)
            }
        }
    }
}
