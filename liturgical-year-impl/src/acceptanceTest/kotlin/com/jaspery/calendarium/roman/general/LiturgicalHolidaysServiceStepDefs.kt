/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.roman.general

import com.jaspery.calendarium.calendar.season.LiturgicalHolidaysService
import com.jaspery.calendarium.calendar.season.model.LiturgicalHoliday
import io.cucumber.guice.ScenarioScoped
import io.cucumber.java8.En
import org.assertj.core.api.Assertions.assertThat
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.threeten.bp.LocalDate
import javax.inject.Inject

@Suppress("unused")
class LiturgicalHolidaysServiceStepDefs @Inject constructor(context: LiturgicalHolidaysCukesContext) : En {
    init {
        with(context) {
            Given("Liturgical Holidays interface is initialized") { assertThat(service).isNotNull() }

            When("date passed to liturgical holidays service is {local_date}") { date: LocalDate ->
                result = service.getLiturgicalHoliday(date)
            }

            Then("liturgical day is not null") {
                assertThat(result).isNotNull()
            }
        }
    }

    @ScenarioScoped
    class LiturgicalHolidaysCukesContext private constructor(
            var result: LiturgicalHoliday?
    ) : KoinComponent {
        constructor() : this(null)

        val service: LiturgicalHolidaysService by inject()
    }
}
