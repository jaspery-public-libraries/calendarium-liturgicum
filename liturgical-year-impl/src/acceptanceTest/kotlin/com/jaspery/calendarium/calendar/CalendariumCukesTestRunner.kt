/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar

import io.cucumber.testng.AbstractTestNGCucumberTests
import io.cucumber.testng.CucumberOptions
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.logger.EmptyLogger
import org.testng.annotations.AfterClass
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test

@Test
@CucumberOptions(
        features = ["src/acceptanceTest/resources/features"],
        glue = ["com.jaspery.calendarium.roman.general", "com.jaspery.calendarium.calendar"],
        tags = "not @ignored",
        plugin = [
            "pretty",
            "html:build/reports/cucumber-reports/cucumber-pretty",
            "json:build/reports/cucumber-reports/CucumberTestReport.json",
            "rerun:build/reports/cucumber-reports/rerun.txt"
        ])
class CalendariumCukesTestRunner : AbstractTestNGCucumberTests() {
    @BeforeClass
    fun setUpKoin() = startKoin {
        logger(EmptyLogger())
        modules(litYearServicesModule)
    }

    @AfterClass
    fun tearDownKoin() = stopKoin()
}
