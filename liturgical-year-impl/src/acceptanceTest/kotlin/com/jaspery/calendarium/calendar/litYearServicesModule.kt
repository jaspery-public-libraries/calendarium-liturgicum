package com.jaspery.calendarium.calendar

import com.jaspery.calendarium.calendar.dao.LiturgicalHolidaySanctoralSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalHolidayTemporalSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalSeasonSpecCsvDao
import com.jaspery.calendarium.calendar.dao.LiturgicalYearSpecCsvDao
import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser
import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.CalendarFileName.*
import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.Rite.ROMAN
import com.jaspery.calendarium.calendar.resource.ClasspathCalendarResourceBrowser
import com.jaspery.calendarium.calendar.resource.ResourceBundleUtf8ControlProvider
import com.jaspery.calendarium.calendar.rrule.RRuleService
import com.jaspery.calendarium.calendar.rrule.impl.RRuleProcessorFactory
import com.jaspery.calendarium.calendar.rrule.impl.RRuleServiceImpl
import com.jaspery.calendarium.calendar.season.LiturgicalHolidaySpecRepository
import com.jaspery.calendarium.calendar.season.LiturgicalHolidaysService
import com.jaspery.calendarium.calendar.season.LiturgicalSeasonSpecRepository
import com.jaspery.calendarium.calendar.season.LiturgicalSeasonsService
import com.jaspery.calendarium.calendar.season.impl.*
import com.jaspery.calendarium.computus.EasterComputus
import com.jaspery.calendarium.computus.impl.tabular.MapBasedEasterComputus
import org.koin.core.qualifier.named
import org.koin.dsl.module

val litYearServicesModule = module {
    // resource browser
    single<CalendarResourceBrowser> {
        ClasspathCalendarResourceBrowser(javaClass.classLoader, ResourceBundleUtf8ControlProvider.UTF8Control)
    }
    single(named("temporalSeasonsResourceUrl")) { get<CalendarResourceBrowser>().generalCalendarResource(ROMAN, TEMPORAL_SEASONS) }
    single(named("temporalHolidaysResourceUrl")) { get<CalendarResourceBrowser>().generalCalendarResource(ROMAN, TEMPORAL_HOLIDAYS) }
    single(named("sanctoralHolidaysResourceUrl")) { get<CalendarResourceBrowser>().generalCalendarResource(ROMAN, SANCTORAL_HOLIDAYS) }
    single(named("liturgicalYearResourceUrl")) { get<CalendarResourceBrowser>().generalCalendarResource(ROMAN, LITURGICAL_YEAR) }

    // dao
    single { LiturgicalSeasonSpecCsvDao(get(named("temporalSeasonsResourceUrl"))) }
    single { LiturgicalHolidayTemporalSpecCsvDao(get(named("temporalHolidaysResourceUrl"))) }
    single { LiturgicalHolidaySanctoralSpecCsvDao(get(named("sanctoralHolidaysResourceUrl"))) }
    single { LiturgicalYearSpecCsvDao(get(named("liturgicalYearResourceUrl"))) }

    // repository
    single<LiturgicalHolidaySpecRepository> { LiturgicalHolidaySpecRepositoryImpl(get(), get()) }
    single<LiturgicalSeasonSpecRepository> { LiturgicalSeasonSpecRepositoryImpl(get()) }
    single { LiturgicalYearSpecRepository(get()) }
    single { LiturgicalYearRepository(get(), get()) }
    single { LiturgicalSeasonRepository(get(), get()) }

    // repository helpers
    single<EasterComputus> { MapBasedEasterComputus.instance }
    single { RRuleProcessorFactory(get()) }
    single<RRuleService> { RRuleServiceImpl(get()) }

    // service
    single<LiturgicalHolidaysService> { LiturgicalHolidaysServiceImpl(get(), get(), get()) }
    single<LiturgicalSeasonsService> { LiturgicalSeasonsServiceImpl(get(), get(), get()) }
    single { LiturgicalYearService(get()) }
}
