/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar

import com.jaspery.calendarium.calendar.season.model.LiturgicalDayPrecedence
import com.jaspery.calendarium.calendar.season.model.LiturgicalHoliday
import com.jaspery.calendarium.calendar.season.model.LiturgicalSeason
import com.jaspery.threetenbp.ext.LocalDateRange
import com.jaspery.threetenbp.ext.date
import io.cucumber.core.api.TypeRegistry
import io.cucumber.core.api.TypeRegistryConfigurer
import io.cucumber.cucumberexpressions.ParameterType
import io.cucumber.datatable.DataTableType
import io.cucumber.datatable.TableRowTransformer
import org.threeten.bp.LocalDate
import org.threeten.bp.Year
import java.util.*

@Suppress("unused")
class CucumberTypeRegistryConfiguration : TypeRegistryConfigurer {
    override fun configureTypeRegistry(typeRegistry: TypeRegistry?) {
        typeRegistry?.apply {
            defineParameterType(ParameterType(
                    "year", ".*?", Year::class.java) { it: String -> Year.parse(it) }
            )
            defineParameterType(ParameterType(
                    "local_date", DATE_REGEX, LocalDate::class.java) { it: String -> LocalDate.parse(it) }
            )
            defineDataTableType(DataTableType(
                    LiturgicalSeason::class.java, TableRowTransformer { parseLiturgicalSeason(it) }
            ))
            defineDataTableType(DataTableType(
                    LiturgicalHoliday::class.java, TableRowTransformer { parseLiturgicalHoliday(it) }
            ))
        }
    }

    override fun locale(): Locale = Locale.ENGLISH

    private companion object {
        private const val DATE_REGEX = "\\d{4}-[01]\\d-[0-3]\\d"
    }

    private fun parseLiturgicalSeason(litSeasonProps: List<String>): LiturgicalSeason {
        val LIT_SEASON_PROPERTIES_COUNT = 7
        check(litSeasonProps.size == LIT_SEASON_PROPERTIES_COUNT) {
            "Cannot parse ${LiturgicalSeason::class} from list $litSeasonProps"
        }
        return LiturgicalSeason(
                dateRange = LocalDateRange(LocalDate.parse(litSeasonProps[2]), LocalDate.parse(litSeasonProps[3])),
                seasonId = litSeasonProps[0],
                nameEn = litSeasonProps[1],
                liturgicalYear = Year.parse(litSeasonProps[4]),
                startWithVesper = litSeasonProps[5].toBoolean(),
                endBeforeVesper = litSeasonProps[6].toBoolean()
        )
    }


    private fun parseLiturgicalHoliday(litHolidayProps: List<String>): LiturgicalHoliday {
        val LIT_HOLIDAY_PROPERTIES_COUNT = 6
        check(litHolidayProps.size == LIT_HOLIDAY_PROPERTIES_COUNT) {
            "Cannot parse ${LiturgicalHoliday::class} from list $litHolidayProps"
        }
        return LiturgicalHoliday(
                date = litHolidayProps[0].date,
                holidayId = litHolidayProps[1],
                seasonId = litHolidayProps[2],
                weekNo = litHolidayProps[3].toInt(),
                precedence = LiturgicalDayPrecedence.valueOf(litHolidayProps[4], litHolidayProps[5])
        )
    }
}
