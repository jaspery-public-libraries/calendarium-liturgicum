package com.jaspery.calendarium.calendar.dao.csv

import com.jaspery.calendarium.calendar.dao.LiturgicalSeasonSpecCsvDao
import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.CalendarFileName.TEMPORAL_SEASONS
import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.Rite.ROMAN
import com.jaspery.calendarium.calendar.resource.ClasspathCalendarResourceBrowser
import com.jaspery.calendarium.calendar.resource.ResourceBundleUtf8ControlProvider
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test

class LiturgicalSeasonCsvDaoIntegrationTest {
    private val resourceBrowser = ClasspathCalendarResourceBrowser(
            LiturgicalSeasonCsvDaoIntegrationTest::class.java.classLoader,
            ResourceBundleUtf8ControlProvider.UTF8Control)
    private val dataSourceUrl = resourceBrowser.generalCalendarResource(ROMAN, TEMPORAL_SEASONS)
    private val dao = LiturgicalSeasonSpecCsvDao(dataSourceUrl)

    @Test
    fun testGetAll() {
        val result = dao.getAll()
        assertThat(result).hasSize(7)
                .extracting("seasonId")
                .contains("advent", "christmasTime", "ordinaryTime", "lent", "paschalTriduum", "easterTime")
    }
}