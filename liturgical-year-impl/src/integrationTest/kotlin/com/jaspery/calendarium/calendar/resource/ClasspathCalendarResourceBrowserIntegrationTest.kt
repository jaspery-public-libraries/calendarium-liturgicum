/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar.resource

import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.CalendarFileName.*
import com.jaspery.calendarium.calendar.resource.CalendarResourceBrowser.Rite.ROMAN
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.Test
import java.util.*

class ClasspathCalendarResourceBrowserIntegrationTest {
    private val classLoader: ClassLoader = ClasspathCalendarResourceBrowser::class.java.classLoader
    private val calendarResourceBrowser: ClasspathCalendarResourceBrowser = ClasspathCalendarResourceBrowser(classLoader, ResourceBundleUtf8ControlProvider.UTF8Control)

    @Test
    fun testGeneralCalendarResource() {
        val temporalSeasonsUrl = calendarResourceBrowser.generalCalendarResource(ROMAN, TEMPORAL_SEASONS)
        val temporalHolidaysUrl = calendarResourceBrowser.generalCalendarResource(ROMAN, TEMPORAL_HOLIDAYS)
        val sanctoralHolidaysUrl = calendarResourceBrowser.generalCalendarResource(ROMAN, SANCTORAL_HOLIDAYS)

        assertThat(temporalSeasonsUrl.toExternalForm()).endsWith(TEMPORAL_SEASONS_URL)
        assertThat(temporalHolidaysUrl.toExternalForm()).endsWith(TEMPORAL_HOLIDAYS_URL)
        assertThat(sanctoralHolidaysUrl.toExternalForm()).endsWith(SANCTORAL_HOLIDAYS_URL)
    }

    @Test
    fun testGeneralCalendarResourceBundle() {
        System.setProperty("java.util.PropertyResourceBundle.encoding", "UTF-8")
        val bundleUk = calendarResourceBrowser.generalCalendarResourceBundle(ROMAN, TEMPORAL_SEASONS, Locale("uk"))

        assertThat(bundleUk).isNotNull
        assertThat(bundleUk.keySet()).hasSize(7)
        assertThat(bundleUk.getString("advent")).isEqualTo("Адвент")
    }

    companion object {
        private const val TEMPORAL_SEASONS_URL = "calendarium/roman/general/temporal-seasons.csv"
        private const val TEMPORAL_HOLIDAYS_URL = "calendarium/roman/general/temporal-holidays.csv"
        private const val SANCTORAL_HOLIDAYS_URL = "calendarium/roman/general/sanctoral-holidays.csv"
    }
}