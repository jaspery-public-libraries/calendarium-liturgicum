package com.jaspery.calendarium.calendar.rrule.standart

import com.jaspery.calendarium.calendar.rrule.RRuleService
import com.jaspery.calendarium.calendar.rrule.impl.RRuleProcessorFactory
import com.jaspery.calendarium.calendar.rrule.impl.RRuleServiceImpl
import com.jaspery.calendarium.computus.impl.tabular.MapBasedEasterComputus
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import com.jaspery.threetenbp.ext.*
import org.assertj.core.api.Assertions.assertThat
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.Year

/**
 * Determine different dates on which JC King Universe solemnity can fall
 */
class JesusKingUniverseTest {
    companion object {
        private const val RRULE = "FREQ=YEARLY;BYMONTH=11;BYMONTHDAY=20,21,22,23,24,25,26;BYDAY=SU"
        private const val RRULE_ALT = "FREQ=YEARLY;BYDAY=-6SU"
        private val possibleDateRange = MonthDayRange.parse("20.11-26.11")
        private val stMd = "01.11".monthDay
    }

    private val rRuleProcessorFactory = RRuleProcessorFactory(MapBasedEasterComputus.instance)

    private val rruleService: RRuleService = RRuleServiceImpl(rRuleProcessorFactory)

    @Test(dataProvider = "years")
    fun testTwoRules(year: Year, expectedDate: LocalDate) {
        val candidate = rruleService.computeNextDate(stMd.atYear(year), RRULE)
        val candidateAlt = rruleService.computeNextDate(stMd.atYear(year), RRULE_ALT)
        println(candidate)
        assertThat(candidate.monthDay in possibleDateRange)
        assertThat(candidate).isEqualTo(expectedDate)
        assertThat(candidateAlt).isEqualTo(candidate)
    }

    @DataProvider
    fun years() = dataProvider {
        scenario(2010.year, "2010-11-21".date)
        scenario(2011.year, "2011-11-20".date)
        scenario(2012.year, "2012-11-25".date)
        scenario(2013.year, "2013-11-24".date)
        scenario(2014.year, "2014-11-23".date)
        scenario(2015.year, "2015-11-22".date)
        scenario(2016.year, "2016-11-20".date)
        scenario(2017.year, "2017-11-26".date)
        scenario(2018.year, "2018-11-25".date)
        scenario(2019.year, "2019-11-24".date)
        scenario(2020.year, "2020-11-22".date)
    }.testNGDataArray()
}