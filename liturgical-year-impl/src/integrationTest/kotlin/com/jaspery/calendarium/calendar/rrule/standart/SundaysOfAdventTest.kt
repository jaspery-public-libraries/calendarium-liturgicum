package com.jaspery.calendarium.calendar.rrule.standart

import com.jaspery.calendarium.calendar.rrule.impl.RRuleProcessorFactory
import com.jaspery.calendarium.calendar.rrule.impl.toDateTime
import com.jaspery.calendarium.calendar.rrule.impl.toLocalDate
import com.jaspery.calendarium.computus.impl.tabular.MapBasedEasterComputus
import org.dmfs.rfc5545.recur.RecurrenceRule
import org.testng.annotations.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.Month.SEPTEMBER
import org.threeten.bp.ZoneId

class SundaysOfAdventTest {
    companion object {
        private const val ADVENT_START_RRULE = "FREQ=YEARLY;BYDAY=-5SU"
        private const val ADVENT_END_RRULE = "FREQ=YEARLY;BYMONTH=12;BYMONTHDAY=24"
        private const val SUNDAYS_SELECTOR_RRULE = "FREQ=WEEKLY;BYDAY=SU"
    }

    private val rRuleProcessorFactory = RRuleProcessorFactory(MapBasedEasterComputus.instance)

    @Test
    fun testIterateAllSundaysOfAdvent() {
        val year = 2018
        val litYearStDate = LocalDate.of(year - 1, SEPTEMBER, 1)

        val adventStart = rRuleProcessorFactory.createRRuleProcessor(ADVENT_START_RRULE).computeNextDate(litYearStDate, ADVENT_START_RRULE)
        val adventEnd = rRuleProcessorFactory.createRRuleProcessor(ADVENT_END_RRULE).computeNextDate(litYearStDate, ADVENT_END_RRULE)

        println(adventStart)
        println(adventEnd)

        val rr = RecurrenceRule(SUNDAYS_SELECTOR_RRULE, RecurrenceRule.RfcMode.RFC5545_STRICT)
        val it = rr.iterator(adventStart.toDateTime())

        var maxInstances = 100 // limit instances for rules that recur forever

        val result = arrayListOf<LocalDate>()
        while (it.hasNext() && (!rr.isInfinite || maxInstances-- > 0)) {
            val nextInstance = it.nextDateTime().toLocalDate(ZoneId.systemDefault())
            if (nextInstance.isAfter(adventEnd)) {
                break
            }
            result.add(nextInstance)
        }

        println(result)
    }
}