package com.jaspery.calendarium.calendar.rrule.standart

import com.jaspery.calendarium.calendar.rrule.RRuleService
import com.jaspery.calendarium.calendar.rrule.impl.RRuleProcessorFactory
import com.jaspery.calendarium.calendar.rrule.impl.RRuleServiceImpl
import com.jaspery.calendarium.calendar.rrule.impl.toDateTime
import com.jaspery.calendarium.calendar.rrule.impl.toLocalDate
import com.jaspery.calendarium.computus.impl.tabular.MapBasedEasterComputus
import com.jaspery.kotlin.testng.dataprovider.dataProvider
import com.jaspery.threetenbp.ext.atYear
import com.jaspery.threetenbp.ext.date
import com.jaspery.threetenbp.ext.monthDay
import com.jaspery.threetenbp.ext.year
import org.assertj.core.api.Assertions.assertThat
import org.dmfs.rfc5545.DateTime
import org.dmfs.rfc5545.recur.RecurrenceRule
import org.dmfs.rfc5545.recurrenceset.RecurrenceList
import org.dmfs.rfc5545.recurrenceset.RecurrenceRuleAdapter
import org.dmfs.rfc5545.recurrenceset.RecurrenceSet
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.Year
import org.threeten.bp.ZoneId

/**
 * Sunday within the Octave of Christmas (or, if there is no such Sunday, 30 December):
 * The Holy Family of Jesus, Mary, and Joseph – Feast
 */
class HolyFamilyFeastDecemberTest {
    companion object {
        private const val RRULE = "FREQ=YEARLY;BYMONTH=12;BYMONTHDAY=26,27,28,29,30,31;BYDAY=SU,FR"
        private const val EXRULE = "FREQ=YEARLY;BYMONTH=12;BYMONTHDAY=26,27,28,29,31;BYDAY=FR"
    }

    private val rRuleProcessorFactory = RRuleProcessorFactory(MapBasedEasterComputus.instance)

    private val rruleService: RRuleService = RRuleServiceImpl(rRuleProcessorFactory)

    @Test(dataProvider = "years")
    fun testWithExRuleDirect(year: Year, expected: LocalDate) {
        val startMd = "01.12".monthDay
        val start = startMd.atYear(year)
        val startDT = start.toDateTime()

        val rset = RecurrenceSet()
        rset.addInstances(RecurrenceRuleAdapter(RecurrenceRule(RRULE, RecurrenceRule.RfcMode.RFC2445_STRICT)))
        rset.addExceptions(RecurrenceRuleAdapter(RecurrenceRule(EXRULE, RecurrenceRule.RfcMode.RFC2445_STRICT)))
        rset.addExceptions(RecurrenceList(LongArray(1) { _ -> startDT.timestamp }))

        val iterator = rset.iterator(startDT.timeZone, startDT.timestamp)
        val nextTimeStamp = if (iterator.hasNext()) iterator.next() else error("")
        val candidate = DateTime(nextTimeStamp).toLocalDate(ZoneId.systemDefault())

        assertThat(candidate.year.year).isEqualTo(year)
        assertThat(candidate).isNotEqualTo("25.12".monthDay.atYear(year))
        assertThat(candidate).isEqualTo(expected)
    }


    @Test(dataProvider = "years")
    fun testWithExRuleService(year: Year, expected: LocalDate) {
        val startMd = "01.12".monthDay
        val start = startMd.atYear(year)

        val candidate = rruleService.computeNextDate(start, RRULE, EXRULE)

        assertThat(candidate.year.year).isEqualTo(year)
        assertThat(candidate).isNotEqualTo("25.12".monthDay.atYear(year))
        assertThat(candidate).isEqualTo(expected)
    }

    @DataProvider
    fun years() = dataProvider {
        scenario(2010.year, "2010-12-26".date) // Sun
        scenario(2011.year, "2011-12-30".date) // Fri
        scenario(2012.year, "2012-12-30".date) // Sun
        scenario(2013.year, "2013-12-29".date) // Sun
        scenario(2014.year, "2014-12-28".date) // Sun
        scenario(2015.year, "2015-12-27".date) // Sun
        scenario(2016.year, "2016-12-30".date) // Fri
        scenario(2017.year, "2017-12-31".date) // Sun
        scenario(2018.year, "2018-12-30".date) // Sun
        scenario(2019.year, "2019-12-29".date) // Sun
        scenario(2020.year, "2020-12-27".date) // Sun
    }.testNGDataArray()
}
