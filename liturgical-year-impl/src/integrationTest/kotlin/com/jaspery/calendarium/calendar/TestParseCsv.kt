/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.calendar

import com.jaspery.calendarium.calendar.season.LiturgicalSeasonsService
import com.jaspery.calendarium.computus.impl.tabular.parseCsv
import org.testng.annotations.Test
import java.net.URL

class TestParseCsv {
    private val easterDatesResource: URL by lazy {
        checkNotNull(classLoader.getResource(EASTER_DATES_RESOURCE)) {
            "File not found: $EASTER_DATES_RESOURCE. Check if easter-computus-resources is in classpath?"
        }
    }

    private val classLoader get() = LiturgicalSeasonsService::class.java.classLoader

    @Test
    fun testParseCsv() {

        parseCsv(easterDatesResource.openStream().bufferedReader()) { strings ->
            strings.map { println(it.trim()) }
        }
    }

    companion object {
        private const val EASTER_DATES_RESOURCE = "calendarium/roman/general/temporal-seasons.csv"
    }
}